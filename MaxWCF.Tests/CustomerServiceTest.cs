﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.CustomerService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class CustomerServiceTest
    {
        [TestMethod]
        public void GetCustomers()
        {
            var customerServiceClient = new CustomerServiceClient();

            var customers = customerServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);

            Assert.IsNotNull(customers);
            Assert.IsTrue(customers.Length > 0);
        }

        [TestMethod]
        public void GetCustomer()
        {
            var customerServiceClient = new CustomerServiceClient();

            var customers = customerServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
            var customerFromCollection = customers.Last();
            var customerLoadedSeparately = customerServiceClient.Get(customerFromCollection.UserCustomerId);

            Assert.IsNotNull(customerLoadedSeparately);
            Assert.AreEqual(customerLoadedSeparately.AccountNumber, customerFromCollection.AccountNumber);
            Assert.AreEqual(customerLoadedSeparately.ContactName, customerFromCollection.ContactName);
            Assert.AreEqual(customerLoadedSeparately.PhoneNumber, customerFromCollection.PhoneNumber);
            Assert.AreEqual(customerLoadedSeparately.Name, customerFromCollection.Name);
        }

        [TestMethod]
        public void UpdateCustomer()
        {
            var customerServiceClient = new CustomerServiceClient();

            var customers = customerServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);

            // Re-get the single customer so addresses are populated.
            UpdateCustomer(customers.Last().UserCustomerId);
            UpdateCustomer(customers.Last(cust => cust.BillToAddress == null).UserCustomerId);
        }

        private void UpdateCustomer(long customerId)
        {
            var customerServiceClient = new CustomerServiceClient();

            // Re-get the single customer so addresses are populated.
            var customer = customerServiceClient.Get(customerId);
            //var customer = customerServiceClient.Get(111006280208);

            var originalValue = customer.Name;
            var newValue = originalValue + "qqq";

            // Give it a new value.
            customer.Name = newValue;
            customerServiceClient.Update(customer, Constants.cUserIdMaxLiftoff2ClearyString);

            // Reload and make sure the new value is still there.
            var reloadedCustomer = customerServiceClient.Get(customerId);

            // Make sure new value is in place, but the rest are unchanged.
            Assert.IsNotNull(reloadedCustomer);
            Assert.AreEqual(reloadedCustomer.Name, newValue);
            Assert.AreEqual(customer.AccountNumber, reloadedCustomer.AccountNumber);
            Assert.AreEqual(customer.ContactName, reloadedCustomer.ContactName);
            Assert.AreEqual(customer.PhoneNumber, reloadedCustomer.PhoneNumber);

            // All good?  Put the original value back.
            customer.Name = originalValue;
            customerServiceClient.Update(customer, Constants.cUserIdMaxLiftoff2ClearyString);

            // Reload and make sure the original name has returned.
            reloadedCustomer = customerServiceClient.Get(customerId);

            // Make sure original value is back, but the rest are unchanged.
            Assert.IsNotNull(reloadedCustomer);
            Assert.AreEqual(reloadedCustomer.Name, originalValue);
            Assert.AreEqual(customer.AccountNumber, reloadedCustomer.AccountNumber);
            Assert.AreEqual(customer.ContactName, reloadedCustomer.ContactName);
            Assert.AreEqual(customer.PhoneNumber, reloadedCustomer.PhoneNumber);
        }

        [TestMethod]
        public void CreateAndDeleteCustomer()
        {
            var customerServiceClient = new CustomerServiceClient();

            var customers = customerServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
            var customer = customers.Last();

            // Get a new value.
            const string cCopyString = " test copy ";
            var newValue = customer.Name;
            var copyIndex = newValue.IndexOf(cCopyString);
            if (copyIndex > 0)
                newValue = newValue.Substring(0, copyIndex);
            newValue += (cCopyString + DateTime.Now);

            // Clear Id and use the new value.
            customer.UserCustomerId = 0;
            customer.Name = newValue;
            var newId = customerServiceClient.Update(customer, Constants.cUserIdMaxLiftoff2ClearyString);

            // Reload and make sure the new value is still there.
            var reloadedCustomer = customerServiceClient.Get(newId);

            // Make sure new record has the same values except one.
            Assert.IsNotNull(reloadedCustomer);
            Assert.AreEqual(reloadedCustomer.Name, newValue);
            Assert.AreEqual(newId, reloadedCustomer.UserCustomerId);
            Assert.AreEqual(customer.AccountNumber, reloadedCustomer.AccountNumber);
            Assert.AreEqual(customer.ContactName, reloadedCustomer.ContactName);
            Assert.AreEqual(customer.PhoneNumber, reloadedCustomer.PhoneNumber);

            // All good?  Now delete.
            Assert.IsTrue(customerServiceClient.Delete(newId));

            // Reload list and make sure the new Id is gone.
            var reloadedCustomers = customerServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);

            Assert.IsFalse(reloadedCustomers.Any(bp => bp.UserCustomerId == newId));
        }
    }
}
