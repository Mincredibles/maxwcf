﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.BlueprintService;
using MaxWCF.Tests.BlueprintPageService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class BlueprintPageServiceTest
    {
        [TestMethod]
        public void GetBlueprintPages()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();
            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);

            Assert.IsNotNull(blueprintPages);
            Assert.IsTrue(blueprintPages.Length > 0);
        }

        [TestMethod]
        public void GetBlueprintPage()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();
            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);
            var blueprintPageFromCollection = blueprintPages.Last();

            var blueprintPageLoadedSeparately = blueprintPageServiceClient.Get(blueprintPages.Last().BlueprintPageId);

            Assert.IsNotNull(blueprintPageLoadedSeparately);
            Assert.AreEqual(blueprintPageLoadedSeparately.BlueprintPageId, blueprintPageFromCollection.BlueprintPageId);
            Assert.AreEqual(blueprintPageLoadedSeparately.BlueprintId, blueprintPageFromCollection.BlueprintId);
            Assert.AreEqual(blueprintPageLoadedSeparately.PageNumber, blueprintPageFromCollection.PageNumber);
            Assert.AreEqual(blueprintPageLoadedSeparately.ImageName, blueprintPageFromCollection.ImageName);
            Assert.AreEqual(blueprintPageLoadedSeparately.ImageUUID, blueprintPageFromCollection.ImageUUID);
        }

        [TestMethod]
        public void UpdateBlueprintPage()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();

            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);
            var blueprintPage = blueprintPages.Last();
            int blueprintPageId = blueprintPage.BlueprintPageId;

            var originalValue = blueprintPage.PageNumber;
            var newValue = originalValue * 7;

            // Give it a new value.
            blueprintPage.PageNumber = newValue;
            blueprintPageServiceClient.Update(blueprintPage);

            // Reload and make sure the new value is still there.
            var reloadedBlueprintPage = blueprintPageServiceClient.Get(blueprintPageId);

            // Make sure new value is in place, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprintPage);
            Assert.AreEqual(reloadedBlueprintPage.PageNumber, newValue);
            Assert.AreEqual(blueprintPage.BlueprintPageId, reloadedBlueprintPage.BlueprintPageId);
            Assert.AreEqual(blueprintPage.BlueprintId, reloadedBlueprintPage.BlueprintId);
            Assert.AreEqual(blueprintPage.ImageName, reloadedBlueprintPage.ImageName);
            Assert.AreEqual(blueprintPage.ImageUUID, reloadedBlueprintPage.ImageUUID);

            // All good?  Put the original value back.
            blueprintPage.PageNumber = originalValue;
            blueprintPageServiceClient.Update(blueprintPage);

            // Reload and make sure the original name has returned.
            reloadedBlueprintPage = blueprintPageServiceClient.Get(blueprintPageId);

            // Make sure original value is back, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprintPage);
            Assert.AreEqual(reloadedBlueprintPage.PageNumber, originalValue);
            Assert.AreEqual(blueprintPage.BlueprintPageId, reloadedBlueprintPage.BlueprintPageId);
            Assert.AreEqual(blueprintPage.BlueprintId, reloadedBlueprintPage.BlueprintId);
            Assert.AreEqual(blueprintPage.ImageName, reloadedBlueprintPage.ImageName);
            Assert.AreEqual(blueprintPage.ImageUUID, reloadedBlueprintPage.ImageUUID);
        }

        [TestMethod]
        public void CreateAndDeleteBlueprintPage()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();

            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);
            var blueprintPage = blueprintPages.OrderBy(bp => bp.PageNumber).Last();

            // Get a new value.
            var newValue = blueprintPage.PageNumber + 1;

            // Give it a new value.
            blueprintPage.BlueprintPageId = 0;
            blueprintPage.PageNumber = newValue;
            var newId = blueprintPageServiceClient.Update(blueprintPage);

            // Clear Id and use the new value.
            var reloadedBlueprintPage = blueprintPageServiceClient.Get(newId);

            // Make sure new record has the same values except one.
            Assert.IsNotNull(reloadedBlueprintPage);
            Assert.AreEqual(reloadedBlueprintPage.PageNumber, newValue);
            Assert.AreEqual(newId, reloadedBlueprintPage.BlueprintPageId);
            Assert.AreEqual(blueprintPage.BlueprintId, reloadedBlueprintPage.BlueprintId);
            Assert.AreEqual(blueprintPage.ImageName, reloadedBlueprintPage.ImageName);
            Assert.AreEqual(blueprintPage.ImageUUID, reloadedBlueprintPage.ImageUUID);

            // All good?  Now delete.
            Assert.IsTrue(blueprintPageServiceClient.Delete(newId));

            // Reload list and make sure the new Id is gone.
            var reloadedBlueprintPages = blueprintPageServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            Assert.IsFalse(reloadedBlueprintPages.Any(bp => bp.BlueprintPageId == newId));
        }

        // Make sure we can't create a duplicate page.
        [TestMethod]
        [ExpectedException(typeof(System.ServiceModel.FaultException))]
        public void CreateDuplicateBlueprintPage()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();

            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);
            var blueprintPage = blueprintPages.OrderBy(bp => bp.PageNumber).Last();

            // Don't change page number.
            blueprintPage.BlueprintPageId = 0;
            blueprintPageServiceClient.Update(blueprintPage);
        }
    }
}