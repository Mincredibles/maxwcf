﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.MerchantContactConfigureOneService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class MerchantContactConfigureOneServiceTest
    {
        [TestMethod]
        public void RetrieveConfiguratorSignOnUrl()
        {
            var merchantContactConfigureOneServiceClient = new MerchantContactConfigureOneServiceClient();

            var ssoUrl = merchantContactConfigureOneServiceClient.RetrieveConfiguratorSignOnUrl(Constants.cContactConfigureOneIdMaxLiftoff2Cleary);

            Assert.IsNotNull(ssoUrl);
        }
    }
}
