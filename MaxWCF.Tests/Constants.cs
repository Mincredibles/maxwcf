﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxWCF.Tests
{
    public class Constants
    {
        public const int cUserIdMaxLiftoff1BWI = 299053909;
        public const int cUserIdMaxLiftoff2 = 205494209;
        public const int cUserIdMaxLiftoff2Cleary = 546120409;
        public const string cUserIdMaxLiftoff2ClearyString = "546120409";
        public const int cContactConfigureOneIdMaxLiftoff2Cleary = 2406;
    }
}
