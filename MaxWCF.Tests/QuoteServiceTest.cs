﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.QuoteService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class QuoteServiceTest
    {
        [TestMethod]
        public void GetQuotes()
        {
            var QuoteServiceClient = new QuoteServiceClient();

            var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);

            Assert.IsNotNull(Quotes);
            Assert.IsTrue(Quotes.Length > 0);
        }

        [TestMethod]
        public void GetQuote()
        {
            var QuoteServiceClient = new QuoteServiceClient();

            var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
            var QuoteFromCollection = Quotes.Last();
            var QuoteLoadedSeparately = QuoteServiceClient.Get(QuoteFromCollection.QuoteId);

            Assert.IsNotNull(QuoteLoadedSeparately);
            Assert.AreEqual(QuoteLoadedSeparately.QuoteId, QuoteFromCollection.QuoteId);
            Assert.AreEqual(QuoteLoadedSeparately.UserCustomerName, QuoteFromCollection.UserCustomerName);
            Assert.AreEqual(QuoteLoadedSeparately.ShippingType, QuoteFromCollection.ShippingType);
            Assert.AreEqual(QuoteLoadedSeparately.Name, QuoteFromCollection.Name);
        }

        [TestMethod]
        public void UpdateQuote()
        {
            var QuoteServiceClient = new QuoteServiceClient();

            var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
            var Quote = Quotes.Last();
            var QuoteId = Quote.QuoteId;

            var originalValue = Quote.Name;
            var newValue = originalValue + "qqq";

            // Give it a new value.
            Quote.Name = newValue;
            QuoteServiceClient.Update(Quote);

            // Reload and make sure the new value is still there.
            var reloadedQuote = QuoteServiceClient.Get(QuoteId);

            // Make sure new value is in place, but the rest are unchanged.
            Assert.IsNotNull(reloadedQuote);
            Assert.AreEqual(reloadedQuote.Name, newValue);
            Assert.AreEqual(Quote.QuoteId, reloadedQuote.QuoteId);
            Assert.AreEqual(Quote.ShippingType, reloadedQuote.ShippingType);
            Assert.AreEqual(Quote.UserCustomerName, reloadedQuote.UserCustomerName);

            // All good?  Put the original value back.
            Quote.Name = originalValue;
            QuoteServiceClient.Update(Quote);

            // Reload and make sure the original name has returned.
            reloadedQuote = QuoteServiceClient.Get(QuoteId);

            // Make sure original value is back, but the rest are unchanged.
            Assert.IsNotNull(reloadedQuote);
            Assert.AreEqual(reloadedQuote.Name, originalValue);
            Assert.AreEqual(Quote.QuoteId, reloadedQuote.QuoteId);
            Assert.AreEqual(Quote.ShippingType, reloadedQuote.ShippingType);
            Assert.AreEqual(Quote.UserCustomerName, reloadedQuote.UserCustomerName);
        }

        //[TestMethod]
        //public void CreateAndDeleteQuote()
        //{
        //    var QuoteServiceClient = new QuoteServiceClient();

        //    var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
        //    var Quote = Quotes.Last();

        //    // Get a new value.
        //    const string cCopyString = " test copy ";
        //    var newValue = Quote.Name;
        //    var copyIndex = newValue.IndexOf(cCopyString);
        //    if (copyIndex > 0)
        //        newValue = newValue.Substring(0, copyIndex);
        //    newValue += (cCopyString + DateTime.Now);

        //    // Clear Id and use the new value.
        //    Quote.QuoteId = 0;
        //    Quote.Name = newValue;
        //    var newId = QuoteServiceClient.Update(Quote);

        //    // Reload and make sure the new value is still there.
        //    var reloadedQuote = QuoteServiceClient.Get(newId);

        //    // Make sure new record has the same values except one.
        //    Assert.IsNotNull(reloadedQuote);
        //    Assert.AreEqual(reloadedQuote.Name, newValue);
        //    Assert.AreEqual(newId, reloadedQuote.QuoteId);
        //    Assert.AreEqual(Quote.ShippingType, reloadedQuote.ShippingType);
        //    Assert.AreEqual(Quote.UserCustomerName, reloadedQuote.UserCustomerName);

        //    // All good?  Now delete.
        //    Assert.IsTrue(QuoteServiceClient.Delete(newId));

        //    // Reload list and make sure the new Id is gone.
        //    var reloadedQuotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);

        //    Assert.IsFalse(reloadedQuotes.Any(bp => bp.QuoteId == newId));
        //}
    }
}
