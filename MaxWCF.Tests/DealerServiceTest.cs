﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.DealerService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class DealerServiceTest
    {
        [TestMethod]
        public void EmptyImageShouldNotUpdateImageFieldTest()
        {
            // Arrange
            var dealerService = new DealerServiceClient();

            var MerchantDistributorDealerId = 669235110;
            var image = string.Empty;

            // Act
            var result = dealerService.Update(MerchantDistributorDealerId, image);

            // Assert
            Assert.IsFalse(result);

        }

        [TestMethod]
        public void ValidImageShouldUpdateImageFieldTest()
        {
            // Arrange
            var dealerService = new DealerServiceClient();

            var MasoniteId = 535027;
            var image = "Cleary%20Color%20Logo.jpg";

            // Act
            var result = dealerService.Update(MasoniteId, image);

            // Assert
            Assert.IsTrue(result);

        }
    }
}
