﻿using System;
using System.Linq;
using MaxWCF.Tests.AnnouncementService;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MaxWCF.Tests
{
    /*
    [OperationContract]
    List<Announcement> GetAnnouncements(string userId, int rowsToReturn);

    [OperationContract]
    bool UpdateAnnouncement(Announcement announcement);

    [OperationContract]
    bool UpdateAnnouncementTitleAndHtml(Announcement announcement);

    [OperationContract]
    bool UpdateAnnouncementShowHide(Announcement announcement); 
     
    */

    [TestClass]
    public class AnnouncementServiceTest
    {
        [TestMethod]
        public void InsertThreeAnnouncements()
        {
            var announcementClient = new AnnouncementsServiceClient();

            //var announcements = announcementClient.GetAnnouncements("272575808", 10);
            var announcement = new Announcement();
            var response = false;

            // RECORD DOES NOT YET EXISTS SO ADD IT ...
            announcement.WorkgroupId = 69575808;
            announcement.Title = "Modern Door Design Workshop";
            announcement.ImagePath = "/images/announcements/modern-door-design-workshop.jpeg";
            announcement. ForEveryone = true;
            announcement.AnnouncementHtml = "<p>Modern door design introduction paragraph.</p>";
            announcement.AnnouncementFullHtml = "<p>Modern door design introduction paragraph. Plus all the details that expand the introduction.</p>";
            announcement.ShowAnnouncement = true;
                
            response = announcementClient.InsertAnnouncement(announcement);

            Assert.IsTrue(response);
            
            //announcements = announcementClient.GetAnnouncements("393053909", 10);

            // RECORD DOES NOT YET EXISTS SO ADD IT ...
            announcement.WorkgroupId = 71504811;
            announcement.Title = "Energy Efficiency Workshop";
            announcement.ImagePath = "/images/announcements/energy-efficiency-workshop.jpeg";
            announcement.ForEveryone = true;
            announcement.AnnouncementHtml = "<p>Energy efficiency introduction paragraph.</p>";
            announcement.AnnouncementFullHtml = "<p>Energy efficiency introduction paragraph. Plus all the details that expand the introduction.</p>";
            announcement.ShowAnnouncement = true;

            response = announcementClient.InsertAnnouncement(announcement);

            Assert.IsTrue(response);
            
            //announcements = announcementClient.GetAnnouncements("491145813", 10);

            // RECORD DOES NOT YET EXISTS SO ADD IT ...
            announcement.WorkgroupId = 195145813;
            announcement.Title = "Chicago Builder's Conference";
            announcement.ImagePath = "/images/announcements/chicago-builders-conference.jpeg";
            announcement.ForEveryone = true;
            announcement.AnnouncementHtml = "<p>Chicago Builder's Conference introduction paragraph.</p>";
            announcement.AnnouncementFullHtml = "<p>Chicago Builder's Conference introduction paragraph. Plus all the details that expand the introduction.</p>";
            announcement.ShowAnnouncement = true;

            response = announcementClient.InsertAnnouncement(announcement);

            Assert.IsTrue(response);
        }

        [TestMethod]
        public void InsertSingleAnnouncement()
        {
            var announcementClient = new AnnouncementsServiceClient();

            //var announcements = announcementClient.GetAnnouncements("083402118", 10);
            var announcement = new Announcement();
            var response = false;

            // RECORD DOES NOT YET EXISTS SO ADD IT ...
            announcement.WorkgroupId = 339172513;
            announcement.Title = "Modern Door Design Workshop";
            announcement.ImagePath = "/images/announcements/modern-door-design-workshop.jpeg";
            announcement.ForEveryone = true;
            announcement.AnnouncementHtml = "<p>Modern door design introduction paragraph.</p>";
            announcement.AnnouncementFullHtml = "<p>Modern door design introduction paragraph. Plus all the details that expand the introduction.</p>";
            announcement.ShowAnnouncement = true;

            response = announcementClient.InsertAnnouncement(announcement);

            Assert.IsTrue(response);
        }

        [TestMethod]
        public void GetAnnouncements()
        {
            var announcementClient = new AnnouncementsServiceClient();

            var announcements = announcementClient.GetAnnouncements("272575808", 10);

            Assert.IsNotNull(announcements);
            Assert.IsTrue(announcements.Length > 0);
        }

        //[TestMethod]
        //public void GetQuote()
        //{
        //    var announcementClient = new AnnouncementsServiceClient();

        //    var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
        //    var QuoteFromCollection = Quotes.Last();
        //    var QuoteLoadedSeparately = QuoteServiceClient.Get(QuoteFromCollection.QuoteId);

        //    Assert.IsNotNull(QuoteLoadedSeparately);
        //    Assert.AreEqual(QuoteLoadedSeparately.QuoteId, QuoteFromCollection.QuoteId);
        //    Assert.AreEqual(QuoteLoadedSeparately.UserCustomerName, QuoteFromCollection.UserCustomerName);
        //    Assert.AreEqual(QuoteLoadedSeparately.ShippingType, QuoteFromCollection.ShippingType);
        //    Assert.AreEqual(QuoteLoadedSeparately.Name, QuoteFromCollection.Name);
        //}

        //[TestMethod]
        //public void UpdateQuote()
        //{
        //    var announcementClient = new AnnouncementsServiceClient();

        //    var Quotes = QuoteServiceClient.GetList(Constants.cUserIdMaxLiftoff2ClearyString);
        //    var Quote = Quotes.Last();
        //    var QuoteId = Quote.QuoteId;

        //    var originalValue = Quote.Name;
        //    var newValue = originalValue + "qqq";

        //    // Give it a new value.
        //    Quote.Name = newValue;
        //    QuoteServiceClient.Update(Quote);

        //    // Reload and make sure the new value is still there.
        //    var reloadedQuote = QuoteServiceClient.Get(QuoteId);

        //    // Make sure new value is in place, but the rest are unchanged.
        //    Assert.IsNotNull(reloadedQuote);
        //    Assert.AreEqual(reloadedQuote.Name, newValue);
        //    Assert.AreEqual(Quote.QuoteId, reloadedQuote.QuoteId);
        //    Assert.AreEqual(Quote.ShippingType, reloadedQuote.ShippingType);
        //    Assert.AreEqual(Quote.UserCustomerName, reloadedQuote.UserCustomerName);

        //    // All good?  Put the original value back.
        //    Quote.Name = originalValue;
        //    QuoteServiceClient.Update(Quote);

        //    // Reload and make sure the original name has returned.
        //    reloadedQuote = QuoteServiceClient.Get(QuoteId);

        //    // Make sure original value is back, but the rest are unchanged.
        //    Assert.IsNotNull(reloadedQuote);
        //    Assert.AreEqual(reloadedQuote.Name, originalValue);
        //    Assert.AreEqual(Quote.QuoteId, reloadedQuote.QuoteId);
        //    Assert.AreEqual(Quote.ShippingType, reloadedQuote.ShippingType);
        //    Assert.AreEqual(Quote.UserCustomerName, reloadedQuote.UserCustomerName);
        //}

    }
}
