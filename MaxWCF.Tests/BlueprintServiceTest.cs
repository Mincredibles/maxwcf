﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.BlueprintService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class BlueprintServiceTest
    {
        [TestMethod]
        public void GetBlueprints()
        {
            var blueprintServiceClient = new BlueprintServiceClient();

            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            Assert.IsNotNull(blueprints);
            Assert.IsTrue(blueprints.Length > 0);
        }

        [TestMethod]
        public void GetBlueprint()
        {
            var blueprintServiceClient = new BlueprintServiceClient();

            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);
            var blueprintFromCollection = blueprints.Last();
            var blueprintLoadedSeparately = blueprintServiceClient.Get(blueprintFromCollection.BlueprintId);

            Assert.IsNotNull(blueprintLoadedSeparately);
            Assert.AreEqual(blueprintLoadedSeparately.BlueprintId, blueprintFromCollection.BlueprintId);
            Assert.AreEqual(blueprintLoadedSeparately.BlueprintStatus, blueprintFromCollection.BlueprintStatus);
            Assert.AreEqual(blueprintLoadedSeparately.WorkgroupId, blueprintFromCollection.WorkgroupId);
            Assert.AreEqual(blueprintLoadedSeparately.Name, blueprintFromCollection.Name);
        }

        [TestMethod]
        public void UpdateBlueprint()
        {
            var blueprintServiceClient = new BlueprintServiceClient();

            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);
            var blueprint = blueprints.Last();
            var blueprintId = blueprint.BlueprintId;

            var originalValue = blueprint.Name;
            var newValue = originalValue + "qqq";

            // Give it a new value.
            blueprint.Name = newValue;
            blueprintServiceClient.Update(blueprint, Constants.cUserIdMaxLiftoff2Cleary);

            // Reload and make sure the new value is still there.
            var reloadedBlueprint = blueprintServiceClient.Get(blueprintId);

            // Make sure new value is in place, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprint);
            Assert.AreEqual(reloadedBlueprint.Name, newValue);
            Assert.AreEqual(blueprint.BlueprintId, reloadedBlueprint.BlueprintId);
            Assert.AreEqual(blueprint.BlueprintStatus, reloadedBlueprint.BlueprintStatus);
            Assert.AreEqual(blueprint.WorkgroupId, reloadedBlueprint.WorkgroupId);

            // All good?  Put the original value back.
            blueprint.Name = originalValue;
            blueprintServiceClient.Update(blueprint, Constants.cUserIdMaxLiftoff2Cleary);

            // Reload and make sure the original name has returned.
            reloadedBlueprint = blueprintServiceClient.Get(blueprintId);

            // Make sure original value is back, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprint);
            Assert.AreEqual(reloadedBlueprint.Name, originalValue);
            Assert.AreEqual(blueprint.BlueprintId, reloadedBlueprint.BlueprintId);
            Assert.AreEqual(blueprint.BlueprintStatus, reloadedBlueprint.BlueprintStatus);
            Assert.AreEqual(blueprint.WorkgroupId, reloadedBlueprint.WorkgroupId);
        }

        [TestMethod]
        public void CreateAndDeleteBlueprint()
        {
            var blueprintServiceClient = new BlueprintServiceClient();

            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);
            var blueprint = blueprints.Last();

            // Get a new value.
            const string cCopyString = " test copy ";
            var newValue = blueprint.Name;
            var copyIndex = newValue.IndexOf(cCopyString);
            if (copyIndex > 0)
                newValue = newValue.Substring(0, copyIndex);
            newValue += (cCopyString + DateTime.Now);

            // Clear Id and use the new value.
            blueprint.BlueprintId = 0;
            blueprint.Name = newValue;
            var newId = blueprintServiceClient.Update(blueprint, Constants.cUserIdMaxLiftoff2Cleary);

            // Reload and make sure the new value is still there.
            var reloadedBlueprint = blueprintServiceClient.Get(newId);

            // Make sure new record has the same values except one.
            Assert.IsNotNull(reloadedBlueprint);
            Assert.AreEqual(reloadedBlueprint.Name, newValue);
            Assert.AreEqual(newId, reloadedBlueprint.BlueprintId);
            Assert.AreEqual(blueprint.BlueprintStatus, reloadedBlueprint.BlueprintStatus);
            Assert.AreEqual(blueprint.WorkgroupId, reloadedBlueprint.WorkgroupId);

            // All good?  Now delete.
            Assert.IsTrue(blueprintServiceClient.Delete(newId));

            // Reload list and make sure the new Id is gone.
            var reloadedBlueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            Assert.IsFalse(reloadedBlueprints.Any(bp => bp.BlueprintId == newId));
        }
    }
}
