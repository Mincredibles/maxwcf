﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MaxWCF.Tests.BlueprintService;
using MaxWCF.Tests.BlueprintPageService;
using MaxWCF.Tests.BlueprintItemLocationService;

namespace MaxWCF.Tests
{
    [TestClass]
    public class BlueprintItemLocationServiceTest
    {
        [TestMethod]
        public void GetBlueprintItemLocations()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();
            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);

            var blueprintItemLocationServiceClient = new BlueprintItemLocationServiceClient();
            var blueprintItemLocations = blueprintItemLocationServiceClient.GetList(blueprintPages.Last().BlueprintPageId);

            Assert.IsNotNull(blueprintItemLocations);
            Assert.IsTrue(blueprintItemLocations.Length > 0);
        }

        [TestMethod]
        public void GetBlueprintItemLocation()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();
            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);

            var blueprintItemLocationServiceClient = new BlueprintItemLocationServiceClient();
            var blueprintItemLocations = blueprintItemLocationServiceClient.GetList(blueprintPages.Last().BlueprintPageId);
            var blueprintItemLocationFromCollection = blueprintItemLocations.Last();

            var blueprintItemLocationLoadedSeparately = blueprintItemLocationServiceClient.Get(blueprintItemLocations.Last().BlueprintItemLocationId);

            Assert.IsNotNull(blueprintItemLocationLoadedSeparately);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.BlueprintItemLocationId, blueprintItemLocationFromCollection.BlueprintItemLocationId);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.BlueprintPageId, blueprintItemLocationFromCollection.BlueprintPageId);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.LocationTypeId, blueprintItemLocationFromCollection.LocationTypeId);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.LocationTypeName, blueprintItemLocationFromCollection.LocationTypeName);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.LocationX, blueprintItemLocationFromCollection.LocationX);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.LocationY, blueprintItemLocationFromCollection.LocationY);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.Height, blueprintItemLocationFromCollection.Height);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.Width, blueprintItemLocationFromCollection.Width);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.StatusId, blueprintItemLocationFromCollection.StatusId);
            Assert.AreEqual(blueprintItemLocationLoadedSeparately.StatusTypeName, blueprintItemLocationFromCollection.StatusTypeName);
        }

        [TestMethod]
        public void UpdateBlueprintItemLocation()
        {
            var blueprintServiceClient = new BlueprintServiceClient();
            var blueprints = blueprintServiceClient.GetList(Constants.cUserIdMaxLiftoff2Cleary);

            var blueprintPageServiceClient = new BlueprintPageServiceClient();
            var blueprintPages = blueprintPageServiceClient.GetList(blueprints.Last().BlueprintId);

            var blueprintItemLocationServiceClient = new BlueprintItemLocationServiceClient();
            var blueprintItemLocations = blueprintItemLocationServiceClient.GetList(blueprintPages.Last().BlueprintPageId);
            var blueprintItemLocation = blueprintItemLocations.Last();
            int blueprintItemLocationId = blueprintItemLocation.BlueprintItemLocationId;

            var originalValue = blueprintItemLocation.Height;
            var newValue = originalValue * 7;

            // Give it a new value.
            blueprintItemLocation.Height = newValue;
            blueprintItemLocationServiceClient.Update(blueprintItemLocation);

            // Reload and make sure the new value is still there.
            var reloadedBlueprintItemLocation = blueprintItemLocationServiceClient.Get(blueprintItemLocationId);

            // Make sure new value is in place, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprintItemLocation);
            Assert.AreEqual(reloadedBlueprintItemLocation.Height, newValue);
            Assert.AreEqual(blueprintItemLocation.BlueprintItemLocationId, reloadedBlueprintItemLocation.BlueprintItemLocationId);
            Assert.AreEqual(blueprintItemLocation.BlueprintPageId, reloadedBlueprintItemLocation.BlueprintPageId);
            Assert.AreEqual(blueprintItemLocation.LocationTypeId, reloadedBlueprintItemLocation.LocationTypeId);
            Assert.AreEqual(blueprintItemLocation.LocationTypeName, reloadedBlueprintItemLocation.LocationTypeName);
            Assert.AreEqual(blueprintItemLocation.LocationX, reloadedBlueprintItemLocation.LocationX);
            Assert.AreEqual(blueprintItemLocation.LocationY, reloadedBlueprintItemLocation.LocationY);
            Assert.AreEqual(blueprintItemLocation.Width, reloadedBlueprintItemLocation.Width);
            Assert.AreEqual(blueprintItemLocation.StatusId, reloadedBlueprintItemLocation.StatusId);
            Assert.AreEqual(blueprintItemLocation.StatusTypeName, reloadedBlueprintItemLocation.StatusTypeName);

            // All good?  Put the original value back.
            blueprintItemLocation.Height = originalValue;
            blueprintItemLocationServiceClient.Update(blueprintItemLocation);

            // Reload and make sure the original name has returned.
            reloadedBlueprintItemLocation = blueprintItemLocationServiceClient.Get(blueprintItemLocationId);

            // Make sure original value is back, but the rest are unchanged.
            Assert.IsNotNull(reloadedBlueprintItemLocation);
            Assert.AreEqual(reloadedBlueprintItemLocation.Height, originalValue);
            Assert.AreEqual(blueprintItemLocation.BlueprintItemLocationId, reloadedBlueprintItemLocation.BlueprintItemLocationId);
            Assert.AreEqual(blueprintItemLocation.BlueprintPageId, reloadedBlueprintItemLocation.BlueprintPageId);
            Assert.AreEqual(blueprintItemLocation.LocationTypeId, reloadedBlueprintItemLocation.LocationTypeId);
            Assert.AreEqual(blueprintItemLocation.LocationTypeName, reloadedBlueprintItemLocation.LocationTypeName);
            Assert.AreEqual(blueprintItemLocation.LocationX, reloadedBlueprintItemLocation.LocationX);
            Assert.AreEqual(blueprintItemLocation.LocationY, reloadedBlueprintItemLocation.LocationY);
            Assert.AreEqual(blueprintItemLocation.Width, reloadedBlueprintItemLocation.Width);
            Assert.AreEqual(blueprintItemLocation.StatusId, reloadedBlueprintItemLocation.StatusId);
            Assert.AreEqual(blueprintItemLocation.StatusTypeName, reloadedBlueprintItemLocation.StatusTypeName);
        }
    }
}
