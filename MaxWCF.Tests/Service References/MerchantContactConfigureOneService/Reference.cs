﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MaxWCF.Tests.MerchantContactConfigureOneService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MerchantContactConfigureOne", Namespace="http://schemas.datacontract.org/2004/07/MaxWCF")]
    [System.SerializableAttribute()]
    public partial class MerchantContactConfigureOne : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ConfigureOneUserIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CreatedByField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateCreatedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime DateModifiedField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool IsReceivingCartEmailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MerchantContactIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MerchantDealerIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MerchantDealerNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MerchantDistributorDealerIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MerchantDistributorIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MerchantDistributorNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ModifiedByField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ConfigureOneUserId {
            get {
                return this.ConfigureOneUserIdField;
            }
            set {
                if ((object.ReferenceEquals(this.ConfigureOneUserIdField, value) != true)) {
                    this.ConfigureOneUserIdField = value;
                    this.RaisePropertyChanged("ConfigureOneUserId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CreatedBy {
            get {
                return this.CreatedByField;
            }
            set {
                if ((object.ReferenceEquals(this.CreatedByField, value) != true)) {
                    this.CreatedByField = value;
                    this.RaisePropertyChanged("CreatedBy");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateCreated {
            get {
                return this.DateCreatedField;
            }
            set {
                if ((this.DateCreatedField.Equals(value) != true)) {
                    this.DateCreatedField = value;
                    this.RaisePropertyChanged("DateCreated");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DateModified {
            get {
                return this.DateModifiedField;
            }
            set {
                if ((this.DateModifiedField.Equals(value) != true)) {
                    this.DateModifiedField = value;
                    this.RaisePropertyChanged("DateModified");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool IsReceivingCartEmail {
            get {
                return this.IsReceivingCartEmailField;
            }
            set {
                if ((this.IsReceivingCartEmailField.Equals(value) != true)) {
                    this.IsReceivingCartEmailField = value;
                    this.RaisePropertyChanged("IsReceivingCartEmail");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MerchantContactId {
            get {
                return this.MerchantContactIdField;
            }
            set {
                if ((this.MerchantContactIdField.Equals(value) != true)) {
                    this.MerchantContactIdField = value;
                    this.RaisePropertyChanged("MerchantContactId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MerchantDealerId {
            get {
                return this.MerchantDealerIdField;
            }
            set {
                if ((this.MerchantDealerIdField.Equals(value) != true)) {
                    this.MerchantDealerIdField = value;
                    this.RaisePropertyChanged("MerchantDealerId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantDealerName {
            get {
                return this.MerchantDealerNameField;
            }
            set {
                if ((object.ReferenceEquals(this.MerchantDealerNameField, value) != true)) {
                    this.MerchantDealerNameField = value;
                    this.RaisePropertyChanged("MerchantDealerName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MerchantDistributorDealerId {
            get {
                return this.MerchantDistributorDealerIdField;
            }
            set {
                if ((this.MerchantDistributorDealerIdField.Equals(value) != true)) {
                    this.MerchantDistributorDealerIdField = value;
                    this.RaisePropertyChanged("MerchantDistributorDealerId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MerchantDistributorId {
            get {
                return this.MerchantDistributorIdField;
            }
            set {
                if ((this.MerchantDistributorIdField.Equals(value) != true)) {
                    this.MerchantDistributorIdField = value;
                    this.RaisePropertyChanged("MerchantDistributorId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MerchantDistributorName {
            get {
                return this.MerchantDistributorNameField;
            }
            set {
                if ((object.ReferenceEquals(this.MerchantDistributorNameField, value) != true)) {
                    this.MerchantDistributorNameField = value;
                    this.RaisePropertyChanged("MerchantDistributorName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ModifiedBy {
            get {
                return this.ModifiedByField;
            }
            set {
                if ((object.ReferenceEquals(this.ModifiedByField, value) != true)) {
                    this.ModifiedByField = value;
                    this.RaisePropertyChanged("ModifiedBy");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="MerchantContactConfigureOneService.IMerchantContactConfigureOneService")]
    public interface IMerchantContactConfigureOneService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveMerchantContactCon" +
            "figureOnes", ReplyAction="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveMerchantContactCon" +
            "figureOnesResponse")]
        MaxWCF.Tests.MerchantContactConfigureOneService.MerchantContactConfigureOne[] RetrieveMerchantContactConfigureOnes(int merchantContactId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveMerchantContactCon" +
            "figureOnes", ReplyAction="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveMerchantContactCon" +
            "figureOnesResponse")]
        System.Threading.Tasks.Task<MaxWCF.Tests.MerchantContactConfigureOneService.MerchantContactConfigureOne[]> RetrieveMerchantContactConfigureOnesAsync(int merchantContactId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveConfiguratorSignOn" +
            "Url", ReplyAction="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveConfiguratorSignOn" +
            "UrlResponse")]
        string RetrieveConfiguratorSignOnUrl(int configureOneId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveConfiguratorSignOn" +
            "Url", ReplyAction="http://tempuri.org/IMerchantContactConfigureOneService/RetrieveConfiguratorSignOn" +
            "UrlResponse")]
        System.Threading.Tasks.Task<string> RetrieveConfiguratorSignOnUrlAsync(int configureOneId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IMerchantContactConfigureOneServiceChannel : MaxWCF.Tests.MerchantContactConfigureOneService.IMerchantContactConfigureOneService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MerchantContactConfigureOneServiceClient : System.ServiceModel.ClientBase<MaxWCF.Tests.MerchantContactConfigureOneService.IMerchantContactConfigureOneService>, MaxWCF.Tests.MerchantContactConfigureOneService.IMerchantContactConfigureOneService {
        
        public MerchantContactConfigureOneServiceClient() {
        }
        
        public MerchantContactConfigureOneServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MerchantContactConfigureOneServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MerchantContactConfigureOneServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MerchantContactConfigureOneServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public MaxWCF.Tests.MerchantContactConfigureOneService.MerchantContactConfigureOne[] RetrieveMerchantContactConfigureOnes(int merchantContactId) {
            return base.Channel.RetrieveMerchantContactConfigureOnes(merchantContactId);
        }
        
        public System.Threading.Tasks.Task<MaxWCF.Tests.MerchantContactConfigureOneService.MerchantContactConfigureOne[]> RetrieveMerchantContactConfigureOnesAsync(int merchantContactId) {
            return base.Channel.RetrieveMerchantContactConfigureOnesAsync(merchantContactId);
        }
        
        public string RetrieveConfiguratorSignOnUrl(int configureOneId) {
            return base.Channel.RetrieveConfiguratorSignOnUrl(configureOneId);
        }
        
        public System.Threading.Tasks.Task<string> RetrieveConfiguratorSignOnUrlAsync(int configureOneId) {
            return base.Channel.RetrieveConfiguratorSignOnUrlAsync(configureOneId);
        }
    }
}
