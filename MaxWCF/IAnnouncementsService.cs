﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAnnouncementsService" in both code and config file together.
    [ServiceContract]
    public interface IAnnouncementsService
    {
        [OperationContract]
        List<Announcement> GetAnnouncements(string userId, int rowsToReturn, string maxVersion);

        [OperationContract]
        Announcement Get(int announcementId);

        [OperationContract]
        bool InsertAnnouncement(Announcement announcement);

        [OperationContract]
        bool UpdateAnnouncement(Announcement announcement);

        [OperationContract]
        bool UpdateAnnouncementTitleAndHtml(Announcement announcement);

        [OperationContract]
        bool UpdateAnnouncementShowHide(Announcement announcement);

    }

    [DataContract]
    public class Announcement
    {
        [DataMember]
        public string UserId { get; set; }
        
        [DataMember]
        public int AnnouncementId { get; set; }

        [DataMember]
        public int WorkgroupId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string ImagePath { get; set; }

        [DataMember]
        public DateTime DatePosted { get; set; }

        [DataMember]
        public bool ForEveryone { get; set; }

        [DataMember]
        public string AnnouncementHtml { get; set; }

        [DataMember]
        public string AnnouncementFullHtml { get; set; }

        [DataMember]
        public bool ShowAnnouncement { get; set; }
    }
}
