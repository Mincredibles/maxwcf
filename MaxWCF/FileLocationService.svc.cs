﻿using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    public class FileLocationService : IFileLocationService
    {
        public void Save(FileLocation fileLoc, string userId)
        {
            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_FileLocation"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterGuid("@uuid", fileLoc.UUID));
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@userId", 100, userId));
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@location", 500, fileLoc.Location));

                SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
            }

        }

        public FileLocation GetFileLocation(Guid uuid)
        {
            FileLocation item = new FileLocation();

            using(System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_FileLocation"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterGuid("@uuid", uuid));

                using(SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    if(reader != null)
                    {
                        while(reader.Read())
                        {

                            item.CreatedBy = reader.GetValueAsString("CreatedBy");
                            item.CreatedDate = reader.GetValueAsDate("CreatedDate");
                            item.Location = reader.GetValueAsString("Location");
                            item.ModifiedBy = reader.GetValueAsString("ModifiedBy");
                            item.ModifiedDate = reader.GetValueAsDate("ModifiedDate");
                        }
                    }
                }
            }

            return item;
        }
    }
}
