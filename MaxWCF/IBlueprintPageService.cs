﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IBlueprintPageService
    {
        [OperationContract]
        List<BlueprintPage> GetList(int blueprintId);

        [OperationContract]
        BlueprintPage Get(int blueprintPageId);

        [OperationContract]
        int Update(BlueprintPage blueprintPage);

        [OperationContract]
        bool Delete(int blueprintPageId);

        [OperationContract]
        bool LinkQuoteDetailtoBlueprintPage(int blueprintPageId, int quoteDetailId);
    }

    [DataContract]
    public class BlueprintPage
    {
        [DataMember]
        public int BlueprintPageId { get; set; }

        [DataMember]
        public int BlueprintId { get; set; }

        [DataMember]
        public int PageNumber { get; set; }

        [DataMember]
        public string ImageName { get; set; }

        [DataMember]
        public Guid ImageUUID { get; set; }
    }
}
