﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class OrderService : IOrderService
    {
        public List<Order> GetOrders(string userId)
        {
            List<Order> items = new List<Order>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Orders"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Order item = new Order();

                            item.Owner.UserId = reader.GetValueAsString("USER_ID");
                            item.OwnerFirstName = reader.GetValueAsString("OWNER_FIRST_NAME");
                            item.OwnerLastName = reader.GetValueAsString("OWNER_LAST_NAME");
                            item.OrderId = reader.GetValueAsInt64("DOC_ID");
                            item.CreatedByUserId = reader.GetValueAsString("CREATED_BY_USER_ID");
                            item.OriginType = reader.GetValueAsString("ORIGIN_TYPE_CD");
                            item.OrderType = reader.GetValueAsString("TYPE_CD");
                            item.CatalogPrice = reader.GetValueAsString("GROSS_PRICE");
                            item.SalePrice = reader.GetValueAsString("NET_PRICE");
                            item.POPrice = reader.GetValueAsString("POPrice");
                            item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                            item.ChildId = reader.GetValueAsInt32("CHILD_ID");
                            item.Currency = reader.GetValueAsString("CURRENCY_CD");
                            item.CurrencyFactor = reader.GetValueAsString("CURRENCY_FACTOR");
                            item.OrderNumber = reader.GetValueAsString("orderNumber");
                            item.OrderRefNumber = reader.GetValueAsString("orderRefNumber");
                            item.TemplateId = reader.GetValueAsInt32("orderTemplateId");
                            item.Status = reader.GetValueAsString("orderStatusCd");
                            item.StatusDescription = reader.GetValueAsString("STATUS_DESC");
                            item.CreateDate = reader.GetValueAsDate("orderCreateDate");
                            item.ModifyDate = reader.GetValueAsDate("orderModifyDate");
                            item.StatusDate = reader.GetValueAsDate("orderStatusDate");
                            item.ProcessDate = reader.GetValueAsDate("orderProcessDate");
                            item.Process = reader.GetValueAsString("orderProcessCd");
                            item.ModifiedByUserId = reader.GetValueAsString("orderModifyUserId");
                            item.SerialNumber = reader.GetValueAsString("SERIAL_NUMBER");
                            item.ProjectId = reader.GetValueAsInt32("PROJECT_ID");
                            item.Name = reader.GetValueAsString("DOC_NAME");
                            item.Locale = reader.GetValueAsString("LOCALE");
                            item.CustomerId = reader.GetValueAsInt64("USER_CUST_ID");
                            item.CustomerName = reader.GetValueAsString("USER_CUST_NAME");
                            item.CustomerBillToAddressId = reader.GetValueAsInt64("USER_CUST_BILL_TO_ADDR_ID");
                            item.CustomerShipToAddressId = reader.GetValueAsInt64("USER_CUST_SHIP_TO_ADDR_ID");
                            item.PONumber = reader.GetValueAsString("PO_NUMBER");
                            item.TaxAmount = reader.GetValueAsString("TAX_AMOUNT");
                            item.ShippingAmount = reader.GetValueAsString("SHIPPING_AMOUNT");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public Order Get(long OrderId)
        {
            Order item = new Order();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Order"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@OrderId", OrderId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                                      // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        item.Owner.UserId = reader.GetValueAsString("USER_ID");
                        item.Owner.FirstName = reader.GetValueAsString("USER_FIRST_NAME");
                        item.Owner.MiddleInitial = reader.GetValueAsString("USER_MIDDLE_INITIAL");
                        item.Owner.LastName = reader.GetValueAsString("USER_LAST_NAME");
                        item.Owner.CompanyName = reader.GetValueAsString("USER_COMPANY_NAME");
                        item.OrderId = reader.GetValueAsInt64("DOC_ID");
                        item.CreatedByUserId = reader.GetValueAsString("CREATED_BY_USER_ID");
                        item.OriginType = reader.GetValueAsString("ORIGIN_TYPE_CD");
                        item.OrderType = reader.GetValueAsString("TYPE_CD");
                        item.CatalogPrice = reader.GetValueAsString("GROSS_PRICE");
                        item.SalePrice = reader.GetValueAsString("NET_PRICE");
                        item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                        item.ChildId = reader.GetValueAsInt32("CHILD_ID");
                        item.Currency = reader.GetValueAsString("CURRENCY_CD");
                        item.CurrencyFactor = reader.GetValueAsString("CURRENCY_FACTOR");
                        item.OrderNumber = reader.GetValueAsString("orderNumber");
                        item.OrderRefNumber = reader.GetValueAsString("orderRefNumber");
                        item.TemplateId = reader.GetValueAsInt32("orderTemplateId");
                        item.Status = reader.GetValueAsString("orderStatusCd");
                        item.StatusDescription = reader.GetValueAsString("STATUS_DESC");
                        item.CreateDate = reader.GetValueAsDate("orderCreateDate");
                        item.ModifyDate = reader.GetValueAsDate("orderModifyDate");
                        item.StatusDate = reader.GetValueAsDate("orderStatusDate");
                        item.ProcessDate = reader.GetValueAsDate("orderProcessDate");
                        item.Process = reader.GetValueAsString("orderProcessCd");
                        item.ModifiedByUserId = reader.GetValueAsString("orderModifyUserId");
                        item.SerialNumber = reader.GetValueAsString("SERIAL_NUMBER");
                        item.ProjectId = reader.GetValueAsInt32("PROJECT_ID");
                        item.Name = reader.GetValueAsString("DOC_NAME");
                        item.Locale = reader.GetValueAsString("LOCALE");
                        item.CustomerId = reader.GetValueAsInt64("USER_CUST_ID");
                        item.CustomerName = reader.GetValueAsString("USER_CUST_NAME");
                        item.CustomerBillToAddressId = reader.GetValueAsInt64("USER_CUST_BILL_TO_ADDR_ID");
                        item.CustomerShipToAddressId = reader.GetValueAsInt64("USER_CUST_SHIP_TO_ADDR_ID");
                        item.PONumber = reader.GetValueAsString("PO_NUMBER");
                        item.TaxAmount = reader.GetValueAsString("TAX_AMOUNT");
                        item.ShippingAmount = reader.GetValueAsString("SHIPPING_AMOUNT");
                    }
                }
            }
            return item;
        }

        public bool Delete(long id, string userId, string description)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_Order"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DocId", id));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@EventDescription", description));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
