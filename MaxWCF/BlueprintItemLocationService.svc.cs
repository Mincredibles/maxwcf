﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class BlueprintItemLocationService : IBlueprintItemLocationService
    {
        public List<BlueprintItemLocation> GetList(int blueprintPageId)
        {
            List<BlueprintItemLocation> items = new List<BlueprintItemLocation>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_BlueprintItemLocations"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintPageId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            BlueprintItemLocation item = new BlueprintItemLocation();

                            item.BlueprintItemLocationId = reader.GetValueAsInt32("Id");
                            item.BlueprintPageId = reader.GetValueAsInt32("BlueprintPageId");
                            item.LocationTypeId = reader.GetValueAsInt32("LocationTypeId");
                            item.LocationTypeName = reader.GetValueAsString("LocationTypeName");
                            item.LocationX = reader.GetValueAsInt32("LocationX");
                            item.LocationY = reader.GetValueAsInt32("LocationY");
                            item.Height = reader.GetValueAsInt32("Height");
                            item.Width = reader.GetValueAsInt32("Width");
                            item.StatusId = reader.GetValueAsInt32("StatusId");
                            item.StatusTypeName = reader.GetValueAsString("StatusTypeName");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public BlueprintItemLocation Get(int blueprintItemLocationId)
        {
            BlueprintItemLocation item = new BlueprintItemLocation();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_BlueprintItemLocation"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintItemLocationId", blueprintItemLocationId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        item.BlueprintItemLocationId = reader.GetValueAsInt32("Id");
                        item.BlueprintPageId = reader.GetValueAsInt32("BlueprintPageId");
                        item.LocationTypeId = reader.GetValueAsInt32("LocationTypeId");
                        item.LocationTypeName = reader.GetValueAsString("LocationTypeName");
                        item.LocationX = reader.GetValueAsInt32("LocationX");
                        item.LocationY = reader.GetValueAsInt32("LocationY");
                        item.Height = reader.GetValueAsInt32("Height");
                        item.Width = reader.GetValueAsInt32("Width");
                        item.StatusId = reader.GetValueAsInt32("StatusId");
                        item.StatusTypeName = reader.GetValueAsString("StatusTypeName");
                    }
                }
            }

            return item;
        }

        public int Update(BlueprintItemLocation blueprintItemLocation)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_BlueprintItemLocation"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // BlueprintItemLocation Params

                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintItemLocationId", blueprintItemLocation.BlueprintItemLocationId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintItemLocation.BlueprintPageId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationTypeId", blueprintItemLocation.LocationTypeId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationX", blueprintItemLocation.LocationX));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationY", blueprintItemLocation.LocationY));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@Height", blueprintItemLocation.Height));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@Width", blueprintItemLocation.Width));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@StatusId", blueprintItemLocation.StatusId));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        var newId = command.Parameters["@NewId"].Value.ToInt32();
                        if (newId > 0) { blueprintItemLocation.BlueprintItemLocationId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public int Insert(BlueprintItemLocation blueprintItemLocation, int DetailId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_BlueprintItemLocation"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // BlueprintItemLocation Params

                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintItemLocationId", blueprintItemLocation.BlueprintItemLocationId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintItemLocation.BlueprintPageId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationTypeId", blueprintItemLocation.LocationTypeId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationX", blueprintItemLocation.LocationX));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@LocationY", blueprintItemLocation.LocationY));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@Height", blueprintItemLocation.Height));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@Width", blueprintItemLocation.Width));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@StatusId", blueprintItemLocation.StatusId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@DetailId", DetailId));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        var newId = command.Parameters["@NewId"].Value.ToInt32();
                        if (newId > 0) { blueprintItemLocation.BlueprintItemLocationId = newId; }

                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            finally
            {

            }
        }

        public bool Delete(int blueprintItemLocationId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_BlueprintItemLocation"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintItemLocationId", blueprintItemLocationId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
