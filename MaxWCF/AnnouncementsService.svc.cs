﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class AnnouncementsService : IAnnouncementsService
    {
        public List<Announcement> GetAnnouncements(string userId, int rowsToReturn, string maxVersion)
        {
            var announcements = new List<Announcement>();

            using (var command = new SqlCommand("MT_GET_ANNOUNCEMENTS_FOR_USER"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@USER_ID", 12, userId));
                command.Parameters.Add(SqlFactory.BuildParameterInt("@rows_to_return", rowsToReturn));
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@maxVersion", 5, maxVersion));
                
                // Execute
                using (var reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            var item = new Announcement();

                            item.UserId = userId;
                            item.AnnouncementId = reader.GetValueAsInt32("ANNOUNCEMENT_ID");
                            item.Title = reader.GetValueAsString("TITLE");
                            item.ImagePath = reader.GetValueAsString("IMAGE_PATH");
                            item.DatePosted = reader.GetValueAsDate("POSTED");
                            item.AnnouncementHtml = reader.GetValueAsString("ANNOUNCEMENT_HTML");
                            item.AnnouncementFullHtml = reader.GetValueAsString("ANNOUNCEMENT_FULL_HTML");
                            item.ShowAnnouncement = reader.GetValueAsBoolean("IS_SHOW_ANNOUNCEMENT");
                            item.WorkgroupId = reader.GetValueAsInt32("WORKGROUP_ID"); 
                            item.ForEveryone = true; // SPROC TAKES CARE OF THIS ... ALWAYS TRUE
                            
                            // Add to collection
                            announcements.Add(item);
                        }
                    }
                }
            }

            return announcements;
        }

        public Announcement Get(int announcementId)
        {
            var announcement = new Announcement();

            using (var command = new SqlCommand("usp_get_Announcement"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@announcementId", announcementId));

                // Execute
                using (var reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    if ((reader != null) && reader.Read())
                    {
                        announcement.AnnouncementId = reader.GetValueAsInt32("ANNOUNCEMENT_ID");
                        announcement.Title = reader.GetValueAsString("TITLE");
                        announcement.ImagePath = reader.GetValueAsString("IMAGE_PATH");
                        announcement.DatePosted = reader.GetValueAsDate("POSTED");
                        announcement.AnnouncementHtml = reader.GetValueAsString("ANNOUNCEMENT_HTML");
                        announcement.AnnouncementFullHtml = reader.GetValueAsString("ANNOUNCEMENT_FULL_HTML");
                        announcement.ShowAnnouncement = reader.GetValueAsBoolean("IS_SHOW_ANNOUNCEMENT");
                        announcement.WorkgroupId = reader.GetValueAsInt32("WORKGROUP_ID");
                        announcement.ForEveryone = reader.GetValueAsBoolean("EVERYONE"); 
                    }
                }
            }
            return announcement;
        }

        public bool InsertAnnouncement(Announcement announcement)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("MT_ADD_ANNOUNCEMENT_FOR_WORKGROUP"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // announcement Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Title", 200, announcement.Title));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ImagePath", 200, announcement.ImagePath));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementHtml", announcement.AnnouncementHtml, true));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementFullHtml", announcement.AnnouncementFullHtml, true));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@ShowAnnouncement", announcement.ShowAnnouncement));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@WorkgroupId", announcement.WorkgroupId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@Everyone", announcement.ForEveryone));
                    
                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool UpdateAnnouncement(Announcement announcement)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("MT_UPDATE_ANNOUNCEMENT_FOR_WORKGROUP"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    // announcement Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@AnnouncementId", announcement.AnnouncementId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Title", 200, announcement.Title));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ImagePath", 200, announcement.ImagePath));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementHtml", announcement.AnnouncementHtml, true));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementFullHtml", announcement.AnnouncementFullHtml, true));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@ShowAnnouncement", announcement.ShowAnnouncement));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@WorkgroupId", announcement.WorkgroupId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@Everyone", announcement.ForEveryone));

                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool UpdateAnnouncementTitleAndHtml(Announcement announcement)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("MT_UPDATE_ANNOUNCEMENT_FOR_WORKGROUP"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // announcement Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@AnnouncementId", announcement.AnnouncementId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Title", 200, announcement.Title));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementHtml", announcement.AnnouncementHtml, true));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@AnnouncementFullHtml", announcement.AnnouncementFullHtml, true));

                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool UpdateAnnouncementShowHide(Announcement announcement)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("MT_UPDATE_ANNOUNCEMENT_SHOW_ANNOUNCEMENT"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // announcement Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@AnnouncementId", announcement.AnnouncementId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@ShowAnnouncement", announcement.ShowAnnouncement));
                    
                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        
    }
}
