﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IQuoteAttachmentUpload" in both code and config file together.
    [ServiceContract]
    public interface IQuoteAttachmentUpload
    {

        [OperationContract]
        int CreateUploadAttachment(string conversationID, string messageId, string filePath, string fileType, string fileName);

    }
}
