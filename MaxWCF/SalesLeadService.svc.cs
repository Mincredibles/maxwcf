﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class SalesLeadService : ISalesLeadService
    {
        public List<SalesLead> GetSalesLeads(string workGroupId)
        {
            var leads = new List<SalesLead>();

            using (var command = new SqlCommand("MT_GET_SALES_LEADS_BY_USER_WORKGROUP"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@workgroup_id", 12, workGroupId));

                // Execute
                using (var reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            var item = new SalesLead();

                            item.WorkGroupId = workGroupId;
                            item.Description = reader.GetValueAsString("customer_name");
                            item.DesignId = reader.GetValueAsString("DESIGN_ID");
                            item.ConfiguredDate = reader.GetValueAsDate("configured_date");
                            item.SalesLeadValue = Convert.ToDouble( reader.GetValueAsString("gross_price"));

                            // Add to collection
                            leads.Add(item);
                        }
                    }
                }
            }

            return leads;
        }

        public SalesLeadSummary GetSalesLeadSummary(string workGroupId)
        {
            var summary = new SalesLeadSummary();

            using (var command = new SqlCommand("MT_GET_SALES_LEADS_SUMMARY_DATA"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@workgroup_id", 12, workGroupId));

                // Execute
                using (var reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        summary.SalesLeadCount = reader.GetInt32("count");
                        summary.SalesLeadValue = Convert.ToDouble(reader.GetDecimal("value"));
                        
                        summary.SummaryDisplay = (summary.SalesLeadCount > 0) ? 
                            summary.SalesLeadCount.ToString() + " lead(s) worth " + summary.SalesLeadValue.ToString("C") : //MAX-143 - To get currency convert upto 2 decimal
                            summary.SalesLeadCount.ToString() + " lead(s)";
                            
                    }
                }
            }

            return summary;
        }

        public SalesLead Get(long designId)
        {
            SalesLead item = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_SalesLead"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    item = new SalesLead();

                    // Populate local object
                    if ((reader != null) && reader.Read())
                    {
                        item.DesignId = reader.GetValueAsString("DESIGN_ID");
                        item.ConfiguredDate = reader.GetValueAsDate("CONFIGURED_DATE");
                        item.Description = reader.GetValueAsString("DESIGN_DESC");
                        item.SalesLeadValue = reader.GetValueAsInt64("GROSS_PRICE");
                        item.WorkGroupId = reader.GetValueAsString("WORKGROUP_ID");

                        item.SalesLeadCustomer.UserCustomerId = reader.GetValueAsInt64("USER_CUST_ID");
                        item.SalesLeadCustomer.Name = reader.GetValueAsString("USER_CUST_NAME");
                        item.SalesLeadCustomer.EmailAddress = reader.GetValueAsString("email_address");
                        item.SalesLeadCustomer.BillToAddress.AddressLine1 = reader.GetValueAsString("address_line_1");
                        item.SalesLeadCustomer.BillToAddress.AddressLine2 = reader.GetValueAsString("address_line_2");
                        item.SalesLeadCustomer.BillToAddress.City = reader.GetValueAsString("city");
                        item.SalesLeadCustomer.BillToAddress.State = reader.GetValueAsString("state");
                        item.SalesLeadCustomer.BillToAddress.PostalCode = reader.GetValueAsString("postal_code");
                        item.SalesLeadCustomer.BillToAddress.PhoneNumber = reader.GetValueAsString("phone_number");
                    }
                }
            }
            return item;
        }

        public long CreateQuoteFromSalesLead(long designId, string userId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_QuoteFromSalesLead"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        long newId = command.Parameters["@NewId"].Value.ToInt64();

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
