﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface ICustomerAddressService
    {
        [OperationContract]
        List<CustomerAddress> GetList(long userCustomerId);

        [OperationContract]
        long Update(CustomerAddress customerAddress, string userId);

        [OperationContract]
        bool Delete(long customerAddressId);
    }

    [DataContract]
    public class CustomerAddress
    {
        [DataMember]
        public long CustomerAddressId { get; set; }

        [DataMember]
        public long CustomerId { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }

        [DataMember]
        public string AddressType { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string AddressLine1 { get; set; }

        [DataMember]
        public string AddressLine2 { get; set; }

        [DataMember]
        public string AddressLine3 { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string FaxNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        //[DataMember]
        //public float Latitude { get; set; }

        //[DataMember]
        //public float Longitude { get; set; }
    }
}
