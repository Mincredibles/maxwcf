﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IQuoteDetailService
    {
        [OperationContract]
        List<QuoteDetail> GetList(long quoteId);

        [OperationContract]
        QuoteDetail Get(int quoteDetailId);

        [OperationContract]
        int Update(string userId, QuoteDetail quoteDetail);

        [OperationContract]
        QuoteDetail InsertSKUBasedItem(QuoteDetail quoteDetail, bool applyDiscount);

        [OperationContract]
        List<QuoteDetail> ReorderDetails(long quoteId, int sourceQuoteDetailId, int destinationQuoteDetailId);

        [OperationContract]
        List<QuoteDetailItem> GetQuoteDetailItemList(long quoteId, long detailId);

        [OperationContract]
        string GetWarranty(long designId);

        [OperationContract]
        List<QuoteDetail> Delete(long docId, long detailId, string userId);

        [OperationContract]
        List<QuoteDesignItem> GetDesignItemList(string userId, long designId);

        [OperationContract]
        long CopyQuoteDetail(long designId, string userId, long quoteId, long detailId);
    }


    [DataContract]
    public class QuoteDetail
    {
        public QuoteDetail()
        {
            FileCreatedBy = new User();
        }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long QuoteId { get; set; }

        [DataMember]
        public int QuoteDetailId { get; set; }

        [DataMember]
        public short SequenceNumber { get; set; }

        [DataMember]
        public string SequenceDescription { get; set; }

        [DataMember]
        public string TypeCode { get; set; }

        [DataMember]
        public int TemplateId { get; set; }

        [DataMember]
        public string Quantity { get; set; }

        [DataMember]
        public string ProductRevisionLevelNumber { get; set; }

        [DataMember]
        public string ChildId { get; set; }

        [DataMember]
        public int DesignBuildNumber { get; set; }

        [DataMember]
        public string DiscountAmt { get; set; }

        [DataMember]
        public string DiscountType { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public double UnitPrice { get; set; }

        [DataMember]
        public double UnitCost { get; set; }

        [DataMember]
        public string ReferenceNumber { get; set; }

        [DataMember]
        public int PriceBookId { get; set; }

        [DataMember]
        public bool Taxable { get; set; }

        [DataMember]
        public string LeadTime { get; set; }

        [DataMember]
        public string UserPrice { get; set; }

        [DataMember]
        public double SalePrice { get; set; }

        [DataMember]
        public double ExtendedSalePrice { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string ImageFileName { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public string DoorLocation { get; set; }

        [DataMember]
        public string IsNetUnit { get; set; }

        [DataMember]
        public string FileId { get; set; }

        [DataMember]
        public string FileLocation { get; set; }

        [DataMember]
        public User FileCreatedBy { get; set; }

        [DataMember]
        public bool ApplyDiscount { get; set; }
        [DataMember]
        public bool IncludeMiscOnPurchaseOrder { get; set; }
    }

    [DataContract]
    public class QuoteDetailItem
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long QuoteId { get; set; }

        [DataMember]
        public int QuoteDetailId { get; set; }

        [DataMember]
        public short SequenceNumber { get; set; }

        [DataMember]
        public string SmartpartNumber { get; set; }

        [DataMember]
        public string ItemNumber { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string DespartId { get; set; }

        [DataMember]
        public string DespartParentId { get; set; }

        [DataMember]
        public string UnitPrice { get; set; }

        [DataMember]
        public string UnitCost { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public string DiscountAmount { get; set; }

        [DataMember]
        public string DiscountType { get; set; }

    }

    [DataContract]
    public class QuoteDesignItem
    {
        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long DesignId { get; set; }

        [DataMember]
        public int ProductInputNum { get; set; }

        [DataMember]
        public string InputName { get; set; }

        [DataMember]
        public string InputTypeCd { get; set; }

        [DataMember]
        public string DesignInputVal { get; set; }

        [DataMember]
        public string InputLabel { get; set; }

        [DataMember]
        public int LogicId { get; set; }

        [DataMember]
        public int SeqNum { get; set; }

        [DataMember]
        public string Price { get; set; }

        [DataMember]
        public string Opt { get; set; }

        [DataMember]
        public DateTime Updated { get; set; }

        [DataMember]
        public string ValueDesc { get; set; }

    }
    
}
