﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IBlueprintItemLocationService
    {
        [OperationContract]
        List<BlueprintItemLocation> GetList(int blueprintPageId);

        [OperationContract]
        BlueprintItemLocation Get(int blueprintItemLocationId);

        [OperationContract]
        int Update(BlueprintItemLocation blueprintItemLocation);

        [OperationContract]
        bool Delete(int blueprintItemLocationId);

        [OperationContract]
        int Insert(BlueprintItemLocation blueprintItemLocation, int DetailId);
    }

    [DataContract]
    public class BlueprintItemLocation
    {
        [DataMember]
        public int BlueprintItemLocationId { get; set; }

        [DataMember]
        public int BlueprintPageId { get; set; }

        [DataMember]
        public int LocationTypeId { get; set; }

        [DataMember]
        public string LocationTypeName { get; set; }

        [DataMember]
        public int LocationX { get; set; }

        [DataMember]
        public int LocationY { get; set; }

        [DataMember]
        public int Height { get; set; }

        [DataMember]
        public int Width { get; set; }

        [DataMember]
        public int StatusId { get; set; }

        [DataMember]
        public string StatusTypeName { get; set; }
    }
}
