﻿using Masonite.MTier.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "QuoteAttachmentUpload" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select QuoteAttachmentUpload.svc or QuoteAttachmentUpload.svc.cs at the Solution Explorer and start debugging.
    public class QuoteAttachmentUpload : IQuoteAttachmentUpload
    {
        public int CreateUploadAttachment(string conversationID, string messageId, string filePath, string fileType, string fileName)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_UploadAttachment"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // QuoteDetail Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationID));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, messageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filepath", 255, messageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filetype", 50, messageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@FileName", 50, messageId));


                    // Output for object's new database ID
                    command.Parameters.Add("@returnIntId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        int newId = command.Parameters["@NewId"].Value.ToInt32();
                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            finally
            {

            }
        }


    }
}
