﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class CustomerAddressService : ICustomerAddressService
    {
        public List<CustomerAddress> GetList(long userCustomerId)
        {
            List<CustomerAddress> items = new List<CustomerAddress>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_CustomerAddresses"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", userCustomerId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            CustomerAddress item = new CustomerAddress();

                            item.CustomerAddressId = reader.GetValueAsInt64("id");
                            item.CustomerId = reader.GetValueAsInt64("user_cust_id");
                            item.IsPrimary = reader.GetValueAsBoolean("primary_ind");
                            item.AddressType = reader.GetValueAsString("type_cd");
                            item.ContactName = reader.GetValueAsString("contact_name");
                            item.AddressLine1 = reader.GetValueAsString("address_line_1");
                            item.AddressLine2 = reader.GetValueAsString("address_line_2");
                            item.AddressLine3 = reader.GetValueAsString("address_line_3");
                            item.City = reader.GetValueAsString("city");
                            item.State = reader.GetValueAsString("state");
                            item.Country = reader.GetValueAsString("country");
                            item.PostalCode = reader.GetValueAsString("postal_code");
                            item.PhoneNumber = reader.GetValueAsString("phone_number");
                            item.FaxNumber = reader.GetValueAsString("fax_number");
                            item.EmailAddress = reader.GetValueAsString("email_address");
                            //item.Latitude = reader.GetValueAsFloat("customer_latitude");
                            //item.Longitude = reader.GetValueAsFloat("customer_longitude");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public long Update(CustomerAddress customerAddress, string userId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_CustomerAddress"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", customerAddress.CustomerId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerAddressId", customerAddress.CustomerAddressId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@IsPrimary", customerAddress.IsPrimary));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@AddressType", 1, customerAddress.AddressType));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@ContactName", 500, customerAddress.ContactName));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@AddressLine1", 500, customerAddress.AddressLine1));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@AddressLine2", 500, customerAddress.AddressLine2));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@AddressLine3", 500, customerAddress.AddressLine3));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@City", 100, customerAddress.City));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@State", 100, customerAddress.State));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Country", 100, customerAddress.Country));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PostalCode", 50, customerAddress.PostalCode));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PhoneNumber", 50, customerAddress.PhoneNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@FaxNumber", 50, customerAddress.FaxNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@EmailAddress", 500, customerAddress.EmailAddress));
                    //command.Parameters.Add(SqlFactory.BuildParameterFloat("@Latitude", customerAddress.Latitude));
                    //command.Parameters.Add(SqlFactory.BuildParameterFloat("@Longitude", customerAddress.Longitude));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        long newId = command.Parameters["@NewId"].Value.ToInt64();
                        if (newId > 0) { customerAddress.CustomerAddressId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool Delete(long customerAddressId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_CustomerAddress"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerAddressId", customerAddressId));

                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

    }
}
