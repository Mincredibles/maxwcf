﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class BlueprintPageService : IBlueprintPageService
    {
        public List<BlueprintPage> GetList(int blueprintId)
        {
            List<BlueprintPage> items = new List<BlueprintPage>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_BlueprintPages"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintId", blueprintId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            BlueprintPage item = new BlueprintPage();

                            item.BlueprintPageId = reader.GetValueAsInt32("Id");
                            item.BlueprintId = reader.GetValueAsInt32("BlueprintId");
                            item.PageNumber = reader.GetValueAsInt32("PageNumber");
                            item.ImageName = reader.GetValueAsString("ImageName");
                            item.ImageUUID = reader.GetGuid("ImageUUID");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public BlueprintPage Get(int blueprintPageId)
        {
            BlueprintPage item = new BlueprintPage();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_BlueprintPage"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@BlueprintPageId", blueprintPageId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        item.BlueprintPageId = reader.GetValueAsInt32("Id");
                        item.BlueprintId = reader.GetValueAsInt32("BlueprintId");
                        item.PageNumber = reader.GetValueAsInt32("PageNumber");
                        item.ImageName = reader.GetValueAsString("ImageName");
                        item.ImageUUID = reader.GetGuid("ImageUUID");
                    }
                }
            }

            return item;
        }

        public int Update(BlueprintPage blueprintPage)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_BlueprintPage"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // BlueprintPage Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintPage.BlueprintPageId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintId", blueprintPage.BlueprintId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@PageNumber", blueprintPage.PageNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ImageName", 250, blueprintPage.ImageName));
                    command.Parameters.Add(SqlFactory.BuildParameterGuid("@ImageUUID", blueprintPage.ImageUUID));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        int newId = command.Parameters["@NewId"].Value.ToInt32();
                        if (newId > 0) { blueprintPage.BlueprintPageId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool Delete(int blueprintPageId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_BlueprintPage"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintPageId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Reset
                        //IsActive = false;
                        //IsDirty = false;

                        // Return
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool LinkQuoteDetailtoBlueprintPage(int blueprintPageId, int quoteDetailId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_link_QuoteDetailToBlueprintPage"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintPageId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@DetailId", quoteDetailId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            finally
            {

            }
        }
    }
}
