﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class CustomerService : ICustomerService
    {
        public List<Customer> GetList(string userId)
        {
            List<Customer> items = new List<Customer>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Customers"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Customer item = new Customer();

                            item.UserCustomerId = reader.GetValueAsInt64("id");
                            item.WorkgroupId = reader.GetValueAsString("workgroup_id");
                            item.EmailAddress = reader.GetValueAsString("email_address");
                            item.Name = reader.GetValueAsString("name");
                            item.AccountNumber = reader.GetValueAsString("account_number");
                            item.PaymentTerms = reader.GetValueAsString("payment_terms");
                            item.ShipVia = reader.GetValueAsString("ship_via");
                            item.ShippingTerms = reader.GetValueAsString("shipping_terms");
                            item.DefaultMarkupPercentage = reader.GetValueAsString("default_markup_pct");
                            item.UserDefined1 = reader.GetValueAsString("udf1");
                            item.UserDefined2 = reader.GetValueAsString("udf2");
                            item.UserDefined3 = reader.GetValueAsString("udf3");
                            item.UserDefined4 = reader.GetValueAsString("udf4");
                            item.UserDefined5 = reader.GetValueAsString("udf5");
                            item.UserDefined6 = reader.GetValueAsString("udf6");
                            item.UserDefined7 = reader.GetValueAsString("udf7");
                            item.UserDefined8 = reader.GetValueAsString("udf8");
                            item.UserDefined9 = reader.GetValueAsString("udf9");
                            item.UserDefined10 = reader.GetValueAsString("udf10");
                            item.Status = reader.GetValueAsString("status");
                            item.CommunicationMethod = reader.GetValueAsString("comm_method");
                            item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                            item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                            item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                            item.ContactName = reader.GetValueAsString("contact_name");
                            item.PhoneNumber = reader.GetValueAsString("phone_number");

                            if (item.UserCustomerId != 0)
                            {
                                CustomerAddressService customerAddressService = new CustomerAddressService();
                                List<CustomerAddress> customerAddresses = customerAddressService.GetList(item.UserCustomerId);

                                if (customerAddresses != null)
                                {
                                    item.BillToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "B");
                                    item.PrimaryShipToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "S");
                                    item.AdditionalShipToAddresses = customerAddresses.Where(customerAddress => customerAddress.AddressType == "S" && customerAddress != item.PrimaryShipToAddress).ToList();
                                }
                            }

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public List<Customer> GetListTest(string userId)
        {
            List<Customer> items = new List<Customer>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_customerTEST"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Customer item = new Customer();

                            item.UserCustomerId = reader.GetValueAsInt64("id");
                            item.WorkgroupId = reader.GetValueAsString("workgroup_id");
                            item.EmailAddress = reader.GetValueAsString("email_address");
                            item.Name = reader.GetValueAsString("name");
                            item.AccountNumber = reader.GetValueAsString("account_number");
                            item.PaymentTerms = reader.GetValueAsString("payment_terms");
                            item.ShipVia = reader.GetValueAsString("ship_via");
                            item.ShippingTerms = reader.GetValueAsString("shipping_terms");
                            item.DefaultMarkupPercentage = reader.GetValueAsString("default_markup_pct");
                            item.UserDefined1 = reader.GetValueAsString("udf1");
                            item.UserDefined2 = reader.GetValueAsString("udf2");
                            item.UserDefined3 = reader.GetValueAsString("udf3");
                            item.UserDefined4 = reader.GetValueAsString("udf4");
                            item.UserDefined5 = reader.GetValueAsString("udf5");
                            item.UserDefined6 = reader.GetValueAsString("udf6");
                            item.UserDefined7 = reader.GetValueAsString("udf7");
                            item.UserDefined8 = reader.GetValueAsString("udf8");
                            item.UserDefined9 = reader.GetValueAsString("udf9");
                            item.UserDefined10 = reader.GetValueAsString("udf10");
                            item.Status = reader.GetValueAsString("status");
                            item.CommunicationMethod = reader.GetValueAsString("comm_method");
                            item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                            item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                            item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                            item.ContactName = reader.GetValueAsString("contact_name");
                            item.PhoneNumber = reader.GetValueAsString("phone_number");

                            if (item.UserCustomerId != 0)
                            {
                                CustomerAddressService customerAddressService = new CustomerAddressService();
                                List<CustomerAddress> customerAddresses = customerAddressService.GetList(item.UserCustomerId);

                                if (customerAddresses != null)
                                {
                                    item.BillToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "B");
                                    item.PrimaryShipToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "S");
                                    item.AdditionalShipToAddresses = customerAddresses.Where(customerAddress => customerAddress.AddressType == "S" && customerAddress != item.PrimaryShipToAddress).ToList();
                                }
                            }

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }


        public Customer Get(long userCustomerId)
        {
            Customer item = new Customer();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Customer"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", userCustomerId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        item.UserCustomerId = reader.GetValueAsInt64("id");
                        item.WorkgroupId = reader.GetValueAsString("workgroup_id");
                        item.EmailAddress = reader.GetValueAsString("email_address");
                        item.Name = reader.GetValueAsString("name");
                        item.AccountNumber = reader.GetValueAsString("account_number");
                        item.PaymentTerms = reader.GetValueAsString("payment_terms");
                        item.ShipVia = reader.GetValueAsString("ship_via");
                        item.ShippingTerms = reader.GetValueAsString("shipping_terms");
                        item.DefaultMarkupPercentage = reader.GetValueAsString("default_markup_pct");
                        item.UserDefined1 = reader.GetValueAsString("udf1");
                        item.UserDefined2 = reader.GetValueAsString("udf2");
                        item.UserDefined3 = reader.GetValueAsString("udf3");
                        item.UserDefined4 = reader.GetValueAsString("udf4");
                        item.UserDefined5 = reader.GetValueAsString("udf5");
                        item.UserDefined6 = reader.GetValueAsString("udf6");
                        item.UserDefined7 = reader.GetValueAsString("udf7");
                        item.UserDefined8 = reader.GetValueAsString("udf8");
                        item.UserDefined9 = reader.GetValueAsString("udf9");
                        item.UserDefined10 = reader.GetValueAsString("udf10");
                        item.Status = reader.GetValueAsString("status");
                        item.CommunicationMethod = reader.GetValueAsString("comm_method");
                        item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                        item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                        item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                        item.ContactName = reader.GetValueAsString("contact_name");
                        item.PhoneNumber = reader.GetValueAsString("phone_number");
                    }
                }
            }

            if (item.UserCustomerId != 0)
            {
                CustomerAddressService customerAddressService = new CustomerAddressService();
                List<CustomerAddress> customerAddresses = customerAddressService.GetList(item.UserCustomerId);

                if (customerAddresses != null)
                {
                    item.BillToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "B");
                    item.PrimaryShipToAddress = customerAddresses.FirstOrDefault(customerAddress => customerAddress.IsPrimary && customerAddress.AddressType == "S");
                    item.AdditionalShipToAddresses = customerAddresses.Where(customerAddress => customerAddress.AddressType == "S" && customerAddress != item.PrimaryShipToAddress).ToList();
                }
            }

            return item;
        }

        public long Update(Customer customer, string userId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_Customer"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Customer Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", customer.UserCustomerId));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@WorkgroupId", 50, customer.WorkgroupId));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@EmailAddress", 500, customer.EmailAddress));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Name", 500, customer.Name));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@AccountNumber", 500, customer.AccountNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PaymentTerms", 500, customer.PaymentTerms));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@ShipVia", 500, customer.ShipVia));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@ShippingTerms", 500, customer.ShippingTerms));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@DefaultMarkupPercentage", 50, customer.DefaultMarkupPercentage));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined1", 50, customer.UserDefined1));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined2", 50, customer.UserDefined2));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined3", 50, customer.UserDefined3));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined4", 50, customer.UserDefined4));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined5", 50, customer.UserDefined5));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined6", 50, customer.UserDefined6));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined7", 50, customer.UserDefined7));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined8", 50, customer.UserDefined8));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined9", 50, customer.UserDefined9));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@UserDefined10", 50, customer.UserDefined10));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Status", 2, customer.Status));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CommunicationMethod", 1, customer.CommunicationMethod));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PricingMethod", 30, customer.PricingMethod));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PricingDefault", 30, customer.PricingDefault));
                    command.Parameters.Add(SqlFactory.BuildParameterNChar("@PricingType", 1, customer.PricingType));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@ContactName", 100, customer.ContactName));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PhoneNumber", 50, customer.PhoneNumber));

                    // Bill To Address Params
                    if (customer.BillToAddress != null)
                    {
                        command.Parameters.Add(SqlFactory.BuildParameterBigInt("@BillToCustomerAddressId", customer.BillToAddress.CustomerAddressId, false));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToContactName", 500, customer.BillToAddress.ContactName));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToAddressLine1", 500, customer.BillToAddress.AddressLine1));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToAddressLine2", 500, customer.BillToAddress.AddressLine2));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToAddressLine3", 500, customer.BillToAddress.AddressLine3));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToCity", 100, customer.BillToAddress.City));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToState", 100, customer.BillToAddress.State));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToCountry", 100, customer.BillToAddress.Country));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToPostalCode", 50, customer.BillToAddress.PostalCode));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToPhoneNumber", 50, customer.BillToAddress.PhoneNumber));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToFaxNumber", 50, customer.BillToAddress.FaxNumber));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@BillToEmailAddress", 500, customer.BillToAddress.EmailAddress));
                        //command.Parameters.Add(SqlFactory.BuildParameterFloat("@BillToLatitude", customer.BillToAddress.Latitude));
                        //command.Parameters.Add(SqlFactory.BuildParameterFloat("@BillToLongitude", customer.BillToAddress.Longitude));
                    }

                    if (customer.PrimaryShipToAddress != null)
                    {
                        // Primary Ship To Address Params
                        command.Parameters.Add(SqlFactory.BuildParameterBigInt("@PrimaryShipToCustomerAddressId", customer.PrimaryShipToAddress.CustomerAddressId, false));
                        command.Parameters.Add(SqlFactory.BuildParameterBit("@PrimaryShipToIsPrimary", customer.PrimaryShipToAddress.IsPrimary));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToContactName", 500, customer.PrimaryShipToAddress.ContactName));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToAddressLine1", 500, customer.PrimaryShipToAddress.AddressLine1));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToAddressLine2", 500, customer.PrimaryShipToAddress.AddressLine2));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToAddressLine3", 500, customer.PrimaryShipToAddress.AddressLine3));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToCity", 100, customer.PrimaryShipToAddress.City));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToState", 100, customer.PrimaryShipToAddress.State));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToCountry", 100, customer.PrimaryShipToAddress.Country));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToPostalCode", 50, customer.PrimaryShipToAddress.PostalCode));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToPhoneNumber", 50, customer.PrimaryShipToAddress.PhoneNumber));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToFaxNumber", 50, customer.PrimaryShipToAddress.FaxNumber));
                        command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PrimaryShipToEmailAddress", 500, customer.PrimaryShipToAddress.EmailAddress));
                        //command.Parameters.Add(SqlFactory.BuildParameterFloat("@PrimaryShipToLatitude", customer.PrimaryShipToAddress.Latitude));
                        //command.Parameters.Add(SqlFactory.BuildParameterFloat("@PrimaryShipToLongitude", customer.PrimaryShipToAddress.Longitude));
                    }

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        long newId = command.Parameters["@NewId"].Value.ToInt64();
                        if (newId > 0) { customer.UserCustomerId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool Delete(long userCustomerId, string userId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_Customer"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", userCustomerId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Reset
                        //IsActive = false;
                        //IsDirty = false;

                        // Return
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
