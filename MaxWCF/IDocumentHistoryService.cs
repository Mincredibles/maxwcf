﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IDocumentHistoryService
    {
        [OperationContract]
        List<DocumentHistory> GetDocumentHistory(long userQuoteId);

        [OperationContract]
        bool InsertDocumentHistory(DocumentHistory QuoteHistory);
    }

    [DataContract]
    public class DocumentHistory
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long DocumentId { get; set; }

        [DataMember]
        public DateTime EventDate { get; set; }

        [DataMember]
        public string EventCode { get; set; }

        [DataMember]
        public string EventUserId { get; set; }

        [DataMember]
        public string EventDescription { get; set; }

        [DataMember]
        public string UserFirstName { get; set; }

        [DataMember]
        public string UserLastName { get; set; }
    }
}
