﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface ISKUBasedItemService
    {
        [OperationContract]
        List<SKUBasedItem> GetList(string userId);
    }

    [DataContract]
    public class SKUBasedItem
    {
        [DataMember]
        public string ItemNumber { get; set; }

        [DataMember]
        public string ItemDescription { get; set; }

        [DataMember]
        public string ItemLongDescription { get; set; }

        [DataMember]
        public string ItemExtDesc1 { get; set; }

        [DataMember]
        public string ItemExtDesc2 { get; set; }

        [DataMember]
        public string ItemExtDesc3 { get; set; }

        [DataMember]
        public string ItemUOM { get; set; }

        [DataMember]
        public int ItemSequence { get; set; }

        [DataMember]
        public string ItemImage { get; set; }

        [DataMember]
        public double ItemPrice { get; set; }

        [DataMember]
        public bool ItemTaxable { get; set; }

        [DataMember]
        public int ItemLeadTime { get; set; }

        [DataMember]
        public int ChildCategoryId { get; set; }

        [DataMember]
        public string ChildCategoryDescription { get; set; }

        [DataMember]
        public int ChildCategorySequence { get; set; }

        [DataMember]
        public string ChildCategoryImage { get; set; }

        [DataMember]
        public int ParentCategoryId { get; set; }

        [DataMember]
        public string ParentCategoryDescription { get; set; }
    }
}
