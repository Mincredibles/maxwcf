﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface ICustomerService
    {
        [OperationContract]
        List<Customer> GetList(string userId);

        [OperationContract]
        List<Customer> GetListTest(string userId);

        [OperationContract]
        Customer Get(long userCustomerId);

        [OperationContract]
        long Update(Customer customer, string userId);

        [OperationContract]
        bool Delete(long userCustomerId, string userId);
    }

    [DataContract]
    public class Customer
    {
        public Customer()
        {
            BillToAddress = new CustomerAddress();
            PrimaryShipToAddress = new CustomerAddress();
            AdditionalShipToAddresses = new List<CustomerAddress>();
        }

        [DataMember]
        public long UserCustomerId { get; set; }

        [DataMember]
        public string WorkgroupId { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string AccountNumber { get; set; }

        [DataMember]
        public string PaymentTerms { get; set; }

        [DataMember]
        public string ShipVia { get; set; }

        [DataMember]
        public string ShippingTerms { get; set; }

        [DataMember]
        public string DefaultMarkupPercentage { get; set; }

        [DataMember]
        public string UserDefined1 { get; set; }

        [DataMember]
        public string UserDefined2 { get; set; }

        [DataMember]
        public string UserDefined3 { get; set; }

        [DataMember]
        public string UserDefined4 { get; set; }

        [DataMember]
        public string UserDefined5 { get; set; }

        [DataMember]
        public string UserDefined6 { get; set; }

        [DataMember]
        public string UserDefined7 { get; set; }

        [DataMember]
        public string UserDefined8 { get; set; }

        [DataMember]
        public string UserDefined9 { get; set; }

        [DataMember]
        public string UserDefined10 { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string CommunicationMethod { get; set; }

        [DataMember]
        public string PricingMethod { get; set; }

        [DataMember]
        public string PricingDefault { get; set; }

        [DataMember]
        public string PricingType { get; set; }

        [DataMember]
        public string ContactName { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public CustomerAddress BillToAddress { get; set; }

        [DataMember]
        public CustomerAddress PrimaryShipToAddress { get; set; }

        [DataMember]
        public List<CustomerAddress> AdditionalShipToAddresses { get; set; }
    }
}
