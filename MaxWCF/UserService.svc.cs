﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class UserService : IUserService
    {
        public List<User> GetList(string userId)
        {
            List<User> items = new List<User>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Users"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            User item = new User();

                            item.UserId = reader.GetValueAsString("USER_ID");
                            item.FirstName = reader.GetValueAsString("USER_FIRST_NAME");
                            item.MiddleInitial = reader.GetValueAsString("USER_MIDDLE_INITIAL");
                            item.LastName = reader.GetValueAsString("USER_LAST_NAME");
                            item.CompanyName = reader.GetValueAsString("USER_COMPANY_NAME");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public User Get(string userId)
        {
            User item = new User();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_User"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        item.UserId = reader.GetValueAsString("USER_ID");
                        item.FirstName = reader.GetValueAsString("USER_FIRST_NAME");
                        item.MiddleInitial = reader.GetValueAsString("USER_MIDDLE_INITIAL");
                        item.LastName = reader.GetValueAsString("USER_LAST_NAME");
                        item.CompanyName = reader.GetValueAsString("USER_COMPANY_NAME");
                        item.QuoteDuration = reader.GetValueAsString("QUOTE_DURATION");
                        item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                        item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                        item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                        item.WorgroupDescription = reader.GetValueAsString("WORKGROUP_DESC");
                        item.DealerSupportEmail = reader.GetValueAsString("DealerSupportEmail");
                        item.DealerPOEmail = reader.GetValueAsString("DealerPOEmail");
                        item.ParentWorgroupDescription = reader.GetValueAsString("ParentWorkgroupDescription");
                        item.PriceBookName = reader.GetValueAsString("PriceBookName");
                        item.ParentPriceBookName = reader.GetValueAsString("ParentPriceBookName");
                        item.QuickquoteTemplate = reader.GetValueAsInt64("QuickquoteTemplate");
                    }
                }
            }

            return item;
        }


        
    }
}
