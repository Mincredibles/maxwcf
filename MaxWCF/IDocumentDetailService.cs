﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDocumentDetailService" in both code and config file together.
    [ServiceContract]
    public interface IDocumentDetailService
    {
        [OperationContract]
        DocumentDetail GetDoorSpec(int bpLocationId);

        [OperationContract]
        List<DocumentDetail> GetDoors(int blueprintPageId);

        [OperationContract]
        List<DocumentDetail> GetAllBlueprintDoors(int blueprintId);

        [OperationContract]
        bool UpdateBlueprintDoorStatus(int blueprintPageId, int statusId);
        
    }

    [DataContract]
    public class DocumentDetail
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long DocId { get; set; }

        [DataMember]
        public int DetailId { get; set; }

        [DataMember]
        public int SequenceNumber { get; set; }

        [DataMember]
        public string SequenceDescription { get; set; }

        [DataMember]
        public string TypeCd { get; set; }

        [DataMember]
        public string ReferenceNumber { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string UnitPrice { get; set; }

        [DataMember]
        public string UnitCost { get; set; }

        [DataMember]
        public string Quantity { get; set; }

        [DataMember]
        public string DiscountAmount { get; set; }

        [DataMember]
        public string DiscountType { get; set; }

        [DataMember]
        public int TemplateId { get; set; }

        [DataMember]
        public string ProductRevisionLevelNumber { get; set; }

        [DataMember]
        public int DesignBuildNumber { get; set; }

        [DataMember]
        public string ChildId { get; set; }

        [DataMember]
        public int PriceBookId { get; set; }

        [DataMember]
        public string CartPrice { get; set; }

        [DataMember]
        public int CartPriceBook { get; set; }

        [DataMember]
        public DateTime DateUpdated { get; set; }

        [DataMember]
        public bool IsTaxable { get; set; }

        [DataMember]
        public string LeadTime { get; set; }

        [DataMember]
        public string UserPrice { get; set; }

        [DataMember]
        public int BlueprintLocationId { get; set; }

        [DataMember]
        public int BlueprintPageId { get; set; }

        [DataMember]
        public Guid ImageUuid { get; set; }

        [DataMember]
        public string Width { get; set; }

        [DataMember]
        public string Height { get; set; }

        [DataMember]
        public string Thickness { get; set; }

        [DataMember]
        public string Config { get; set; }

        [DataMember]
        public string Core { get; set; }

        [DataMember]
        public string ProductLine { get; set; }

        [DataMember]
        public string DoorStyle { get; set; }

        [DataMember]
        public string WallDepth { get; set; }

        [DataMember]
        public string Swing { get; set; }
    }
}
