﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IBlueprintService
    {
        [OperationContract]
        List<Blueprint> GetList(string userId);

        [OperationContract]
        Blueprint Get(int blueprintId);

        [OperationContract]
        int Update(Blueprint Blueprint, int userId);

        [OperationContract]
        bool Delete(int blueprintId);

        [OperationContract]
        List<BlueprintScaleType> GetScaleTypes();

        [OperationContract]
        List<Blueprint> GetAllBlueprints();
    }

    [DataContract]
    public class BlueprintScaleType
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class Blueprint
    {
        public Blueprint()
        {
            JobAddress = new CustomerAddress();
        }

        [DataMember]
        public int BlueprintId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool BlueprintStatus { get; set; }

        [DataMember]
        public DateTime DateCreated { get; set; }

        [DataMember]
        public int CreatedBy { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }

        [DataMember]
        public int ModifiedBy { get; set; }

        [DataMember]
        public int WorkgroupId { get; set; }

        [DataMember]
        public string TakeOffType { get; set; }

        [DataMember]
        public string BuilderName { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public DateTime DateCreatedReceivedFromBuilder { get; set; }

        [DataMember]
        public DateTime SubmittalDueDate { get; set; }

        [DataMember]
        public long CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public Guid ImageUUID { get; set; }

        [DataMember]
        public string PagesToAnalyze { get; set; }

        [DataMember]
        public long QuoteId { get; set; }

        [DataMember]
        public long JobSiteAddressId { get; set; }

        [DataMember]
        public CustomerAddress JobAddress { get; set; }

        [DataMember]
        public BlueprintScaleType Scale { get; set; }
        
    }
}
