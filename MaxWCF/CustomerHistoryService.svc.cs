﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class CustomerHistoryService : ICustomerHistoryService
    {
        public List<CustomerHistory> GetCustomerHistory(long userCustomerId)
        {
            List<CustomerHistory> items = new List<CustomerHistory>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_CustomerHistory"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", userCustomerId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            CustomerHistory item = new CustomerHistory();

                            item.UserId = reader.GetValueAsString("USER_ID");
                            item.UserCustomerId = reader.GetValueAsInt64("USER_CUSTOMER_ID");
                            item.EventDate = reader.GetValueAsDate("EVENT_DATE");
                            item.EventCode = reader.GetValueAsString("EVENT_CODE");
                            item.EventUser.UserId = reader.GetValueAsString("EVENT_USER_ID");
                            item.EventUser.UserName = reader.GetValueAsString("EventUserName");
                            item.EventUser.FirstName = reader.GetValueAsString("EventUserFirstName");
                            item.EventUser.MiddleInitial = reader.GetValueAsString("EventUserMiddleInitial");
                            item.EventUser.LastName = reader.GetValueAsString("EventUserLastName");
                            item.EventUser.CompanyName = reader.GetValueAsString("EventUserCompanyName");
                            item.EventDescription = reader.GetValueAsString("EVENT_DESCRIPTION");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public bool InsertCustomerHistory(CustomerHistory customerHistory)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_CustomerHistory"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Customer Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, customerHistory.UserId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", customerHistory.UserCustomerId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@EventCode", 20, customerHistory.EventCode));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@EventUserId", 20, customerHistory.EventUser.UserId));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@EventDescription", customerHistory.EventDescription));
                                                                                                              
                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
