﻿using Masonite.MTier.Utility;

namespace MaxWCF
{
    public class DealerService : IDealerService
    {
        public bool Update(int masoniteId, string image)
        {
            if (string.IsNullOrEmpty(image)) return false;

            // Command
            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_Dealer"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Customer Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@MasoniteId", masoniteId));
                command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Image", 100, image));

                // Execute
                var result = SqlFactory.GetNonQuery(command,
                    System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);

                return result;
            }
        }
    }
}
