﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class QuoteService : IQuoteService
    {
        public List<Quote> GetList(string userId, long customerId)
        {
            return GetQuoteListForLoggedInDealer(userId, customerId, true, false);
        }

        public List<Quote> GetTemplates(string userId, long customerId)
        {
            return GetQuoteListForLoggedInDealer(userId, customerId, true, true);
        }

        public List<Quote> GetMyQuotesList(string userId, long customerId)
        {
            var quotes = GetList(userId, customerId);
            List<Quote> retList = quotes.Where(q => q.Owner.UserId == userId).ToList();
            return retList;
        }

        public List<Quote> GetQuoteListForLoggedInDealer(string userId, long customerId, bool includeWorkGroups)
        {
            return GetQuoteListForLoggedInDealer(userId, customerId, includeWorkGroups, false);
        }

        private List<Quote> GetQuoteListForLoggedInDealer(string userId, long customerId, bool includeWorkGroups, bool onlyTemplates)
        {
            List<Quote> items = new List<Quote>();

            string storedProcedureName = "usp_get_Quotes";  // Original stored procedure

            if (includeWorkGroups)
            {
                storedProcedureName = "usp_get_Quotes_With_WorkGroups";
            }
            else
            {
                storedProcedureName = "usp_get_Quotes_Without_WorkGroups";
            }

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(storedProcedureName))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CreatedByUserId", 12, ""));
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", customerId));
                command.Parameters.Add(SqlFactory.BuildParameterBit("@IncludeWorkgroup", includeWorkGroups));
                command.Parameters.Add(SqlFactory.BuildParameterBit("@OnlyTemplates", onlyTemplates));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Quote item = new Quote();

                            item.Owner.UserId = reader.GetValueAsString("USER_ID");
                            item.QuoteId = reader.GetValueAsInt64("DOC_ID");
                            item.Name = reader.GetValueAsString("DOC_NAME");
                            item.SerialNumber = reader.GetValueAsString("SERIAL_NUMBER");
                            item.BuildNumber = reader.GetValueAsInt16("BUILD_NUMBER");
                            item.Status = reader.GetValueAsString("STATUS_CD");
                            item.CatalogPrice = reader.GetValueAsString("GROSS_PRICE");
                            item.SalePrice = reader.GetValueAsDouble("NET_PRICE");
                            item.DealerPrice = reader.GetValueAsString("DEALER_PRICE");
                            item.ProjectId = reader.GetValueAsInt64("PROJECT_ID");
                            item.CreatedBy.UserId = reader.GetValueAsString("CREATED_BY_USER_ID");
                            item.ModifiedBy.UserId = reader.GetValueAsString("MODIFY_USER_ID");
                            item.OriginType = reader.GetValueAsString("ORIGIN_TYPE_CD");
                            item.QuoteType = reader.GetValueAsString("TYPE_CD");
                            item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                            item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                            item.ChildId = reader.GetValueAsInt64("CHILD_ID");
                            item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                            item.ModifyDate = reader.GetValueAsDate("MODIFY_DATE");
                            item.StatusDate = reader.GetValueAsDate("STATUS_DATE");
                            item.ExpireDate = reader.GetValueAsDate("EXPIRE_DATE");
                            item.ViewDate = reader.GetValueAsDate("VIEW_DATE");
                            item.BuildDate = reader.GetValueAsDate("BUILD_DATE");
                            item.ForecastDate = reader.GetValueAsDate("FORECAST_DATE");
                            item.Probability = reader.GetValueAsString("PROBABILITY_CD");
                            item.OpportunityLost = reader.GetValueAsString("OPPORTUNITY_LOST_CD");
                            item.ForecastOver1 = reader.GetValueAsString("FORECAST_OVR_1");
                            item.ForecastOver2 = reader.GetValueAsString("FORECAST_OVR_2");
                            item.ForecastOver3 = reader.GetValueAsString("FORECAST_OVR_3");
                            item.Currency = reader.GetValueAsString("CURRENCY_CD");
                            item.CurrencyFactor = reader.GetValueAsString("CURRENCY_FACTOR");
                            item.QuoteValid = reader.GetValueAsBoolean("QUOTE_VALID");
                            item.OrderNumber = reader.GetValueAsString("orderNumber");
                            item.OrderRefNumber = reader.GetValueAsString("orderRefNumber");
                            item.Locale = reader.GetValueAsString("LOCALE");
                            item.UserCustomerId = reader.GetValueAsInt64("USER_CUST_ID");
                            item.UserCustomerName = reader.GetValueAsString("USER_CUST_NAME");
                            item.UserCustomerBillToAddressId = reader.GetValueAsInt64("USER_CUST_BILL_TO_ADDR_ID");
                            item.UserCustomerShipToAddressId = reader.GetValueAsInt64("USER_CUST_SHIP_TO_ADDR_ID");
                            item.Archived = reader.GetValueAsBoolean("ARCHIVED");
                            item.ArchivedDate = reader.GetValueAsDate("ARCHIVED_DATE");
                            item.ArchivedBy = reader.GetValueAsString("ARCHIVED_BY");
                            item.Note = reader.GetValueAsString("NOTE");
                            item.ShippingAmt = reader.GetValueAsDouble("SHIPPING_AMT");
                            item.ShippingType = reader.GetValueAsString("SHIPPING_TYPE");
                            item.TaxAmt = reader.GetValueAsDouble("TAX_AMT");
                            item.TaxType = reader.GetValueAsString("TAX_TYPE");
                            item.DepositPercentage = reader.GetValueAsInt32("DEPOSIT_PCT");
                            item.PONumber = reader.GetValueAsString("PO_NUMBER");
                            item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                            item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                            item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                            item.TaxAmount = reader.GetValueAsDouble("TAX_AMOUNT");
                            item.ShippingAmount = reader.GetValueAsDouble("SHIPPING_AMOUNT");
                            item.CreatedBy.UserName = reader.GetValueAsString("CREATED_USER_NAME");
                            item.CreatedBy.FirstName = reader.GetValueAsString("CreatedFirstName");
                            item.CreatedBy.MiddleInitial = reader.GetValueAsString("CreatedMiddleInitial");
                            item.CreatedBy.LastName = reader.GetValueAsString("CreatedLastName");
                            item.CreatedBy.CompanyName = reader.GetValueAsString("CreatedCompanyName");
                            item.ModifiedBy.UserName = reader.GetValueAsString("MODIFY_USER_NAME");
                            item.ModifiedBy.FirstName = reader.GetValueAsString("ModifiedFirstName");
                            item.ModifiedBy.MiddleInitial = reader.GetValueAsString("ModifiedMiddleInitial");
                            item.ModifiedBy.LastName = reader.GetValueAsString("ModifiedLastName");
                            item.ModifiedBy.CompanyName = reader.GetValueAsString("ModifiedCompanyName");
                            item.Owner.UserName = reader.GetValueAsString("OWNER_USER_NAME");
                            item.Owner.FirstName = reader.GetValueAsString("OwnerFirstName");
                            item.Owner.MiddleInitial = reader.GetValueAsString("OwnerMiddleInitial");
                            item.Owner.LastName = reader.GetValueAsString("OwnerLastName");
                            item.Owner.CompanyName = reader.GetValueAsString("OwnerCompanyName");
                            item.BlueprintId = reader.GetValueAsInt32("BlueprintId");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public List<QuickQuote> GetQuickQuotesForLoggedInDealer(string userId)
        {
            List<QuickQuote> items = new List<QuickQuote>();

            using (var command = new System.Data.SqlClient.SqlCommand("MT_GET_QUICK_QUOTE_LINE_ITEMS_FOR_USER"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@USER_ID", 12, userId));
                try
                {
                    // Execute
                    using (var reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Populate local object
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                var numericPrice = 0.0;
                                var item = new QuickQuote();

                                item.UserId = reader.GetValueAsString("userId");
                                item.DetailId = reader.GetValueAsInt32("detailId");
                                item.DesignId = reader.GetValueAsString("designId");
                                item.ReferenceNumber = reader.GetValueAsString("referenceNumber"); // SAME AS SerialNumber
                                item.DocumentId = reader.GetValueAsInt64("documentId");

                                if (!string.IsNullOrEmpty(reader.GetValueAsString("unitPrice")))
                                    numericPrice = Convert.ToDouble(reader.GetValueAsString("unitPrice"));

                                item.UnitPrice = numericPrice.ToString("$#,##0.00");
                                item.Description = reader.GetValueAsString("description");
                                item.DesignUserId = reader.GetValueAsString("designUserId");
                                item.ProductId = reader.GetValueAsInt32("productId");
                                item.DesignInputName = reader.GetValueAsString("desInputName");
                                item.DesignInputValue = reader.GetValueAsString("desInputValue");
                                item.DesignInputNameCatalog = reader.GetValueAsString("desInputNameCatalog");
                                item.DesignInputValueCatalog = reader.GetValueAsString("desInputValueCatalog");

                                // Add to collection
                                items.Add(item);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    string ms = ex.Message;
                    throw;
                }
            }
            return items;
        }

        // MAX-23 Added functionality for getting all the purchase order numbers.
        public List<String> GetOrdersPoList(string userId)
        {
            List<String> items = new List<String>();

            string storedProcedureName = "usp_get_Polist_For_Dealer";  // New stored procedure

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(storedProcedureName))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            String PoNumber = reader.GetValueAsString("PO_NUMBER");

                            // Add to collection
                            items.Add(PoNumber);
                        }
                    }
                }
            }
            return items;
        }


        public Quote Get(long quoteId)
        {
            Quote item = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Quote"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    item = GetQuote(reader);
                }
            }
            return item;
        }

        public Quote Update(string userId, Quote quote)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            Quote item = null;

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_Quote"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Quote Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 20, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OwnerUserId", 12, (quote.Owner != null) ? quote.Owner.UserId : null));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quote.QuoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Name", 500, quote.Name));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@SerialNumber", 40, quote.SerialNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterSmallInt("@BuildNumber", quote.BuildNumber, false));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@Status", 2, quote.Status));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CatalogPrice", 50, quote.CatalogPrice, false));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@SalePrice", quote.SalePrice, false));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@DealerPrice", 50, quote.DealerPrice, false));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@ProjectId", quote.ProjectId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CreatedByUserId", 12, (quote.CreatedBy != null) ? quote.CreatedBy.UserId : null));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ModifiedByUserId", 12, (quote.ModifiedBy != null) ? quote.ModifiedBy.UserId : null));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OrigIntype", 2, quote.OriginType));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@QuoteType", 2, quote.QuoteType));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@TemplateId", quote.TemplateId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@PriceBookId", quote.PriceBookId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@ChildId", quote.ChildId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ModifyDate", quote.ModifyDate));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@StatusDate", quote.StatusDate));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ExpireDate", quote.ExpireDate));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ViewDate", quote.ViewDate));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@BuildDate", quote.BuildDate));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ForecastDate", quote.ForecastDate));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Probability", 10, quote.Probability));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OpportunityLost", 10, quote.OpportunityLost));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ForecastOver1", 10, quote.ForecastOver1));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ForecastOver2", 10, quote.ForecastOver2));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ForecastOver3", 10, quote.ForecastOver3));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Currency", 20, quote.Currency));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CurrencyFactor", 50, quote.CurrencyFactor));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@QuoteValid", quote.QuoteValid));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OrderNumber", 250, quote.OrderNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OrderRefNumber", 250, quote.OrderRefNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Locale", 20, quote.Locale));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerId", quote.UserCustomerId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerBillToAddressId", quote.UserCustomerBillToAddressId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerShipToAddressId", quote.UserCustomerShipToAddressId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@Archived", quote.Archived));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ArchivedDate", quote.ArchivedDate));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ArchivedBy", 12, quote.ArchivedBy));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Note", 5000, quote.Note));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@ShippingAmt", quote.ShippingAmt, false));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@ShippingType", 1, quote.ShippingType));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@TaxAmt", quote.TaxAmt, false));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@TaxType", 1, quote.TaxType));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@DepositPercentage", quote.DepositPercentage));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@PONumber", 250, quote.PONumber));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PricingMethod", 30, quote.PricingMethod));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@PricingDefault", 30, quote.PricingDefault));
                    command.Parameters.Add(SqlFactory.BuildParameterNChar("@PricingType", 1, quote.PricingType));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@TaxAmount", quote.TaxAmount, false));

                    using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        item = GetQuote(reader);
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
            return item;
        }

        private Quote GetQuote(SqlDataReader reader)
        {
            Quote item = new Quote();

            // Populate local object
            if ((reader != null) && reader.Read())
            {
                item.Owner.UserId = reader.GetValueAsString("USER_ID");
                item.Owner.FirstName = reader.GetValueAsString("USER_FIRST_NAME");
                item.Owner.MiddleInitial = reader.GetValueAsString("USER_MIDDLE_INITIAL");
                item.Owner.LastName = reader.GetValueAsString("USER_LAST_NAME");
                item.Owner.CompanyName = reader.GetValueAsString("USER_COMPANY_NAME");
                item.QuoteId = reader.GetValueAsInt64("DOC_ID");
                item.Name = reader.GetValueAsString("DOC_NAME");
                item.SerialNumber = reader.GetValueAsString("SERIAL_NUMBER");
                item.BuildNumber = reader.GetValueAsInt16("BUILD_NUMBER");
                item.Status = reader.GetValueAsString("STATUS_CD");
                item.StatusDescription = reader.GetValueAsString("STATUS_DESC");
                item.CatalogPrice = reader.GetValueAsString("GROSS_PRICE");
                item.SalePrice = reader.GetValueAsDouble("NET_PRICE");
                item.DealerPrice = reader.GetValueAsString("DEALER_PRICE");
                item.ProjectId = reader.GetValueAsInt64("PROJECT_ID");
                item.CreatedBy.UserId = reader.GetValueAsString("CREATED_BY_USER_ID");
                item.ModifiedBy.UserId = reader.GetValueAsString("MODIFY_USER_ID");
                item.OriginType = reader.GetValueAsString("ORIGIN_TYPE_CD");
                item.QuoteType = reader.GetValueAsString("TYPE_CD");
                item.ShipToAddressLine1 = reader.GetValueAsString("SHIP_TO_ADDRESS_LINE_1");
                item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                item.ChildId = reader.GetValueAsInt64("CHILD_ID");
                item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                item.ModifyDate = reader.GetValueAsDate("MODIFY_DATE");
                item.StatusDate = reader.GetValueAsDate("STATUS_DATE");
                item.ExpireDate = reader.GetValueAsDate("EXPIRE_DATE");
                item.ViewDate = reader.GetValueAsDate("VIEW_DATE");
                item.BuildDate = reader.GetValueAsDate("BUILD_DATE");
                item.ForecastDate = reader.GetValueAsDate("FORECAST_DATE");
                item.Probability = reader.GetValueAsString("PROBABILITY_CD");
                item.OpportunityLost = reader.GetValueAsString("OPPORTUNITY_LOST_CD");
                item.ForecastOver1 = reader.GetValueAsString("FORECAST_OVR_1");
                item.ForecastOver2 = reader.GetValueAsString("FORECAST_OVR_2");
                item.ForecastOver3 = reader.GetValueAsString("FORECAST_OVR_3");
                item.Currency = reader.GetValueAsString("CURRENCY_CD");
                item.CurrencyFactor = reader.GetValueAsString("CURRENCY_FACTOR");
                item.QuoteValid = reader.GetValueAsBoolean("QUOTE_VALID");
                item.OrderNumber = reader.GetValueAsString("orderNumber");
                item.OrderRefNumber = reader.GetValueAsString("orderRefNumber");
                item.Locale = reader.GetValueAsString("LOCALE");
                item.UserCustomerId = reader.GetValueAsInt64("USER_CUST_ID");
                item.UserCustomerName = reader.GetValueAsString("USER_CUST_NAME");
                item.UserCustomerAccountNumber = reader.GetValueAsString("USER_CUST_ACCOUNT_NUMBER");
                item.CustomerEmailAddress = reader.GetValueAsString("USER_CUST_EMAIL_ADDRESS");
                item.UserCustomerBillToAddressId = reader.GetValueAsInt64("USER_CUST_BILL_TO_ADDR_ID");
                item.UserCustomerShipToAddressId = reader.GetValueAsInt64("USER_CUST_SHIP_TO_ADDR_ID");
                item.Archived = reader.GetValueAsBoolean("ARCHIVED");
                item.ArchivedDate = reader.GetValueAsDate("ARCHIVED_DATE");
                item.ArchivedBy = reader.GetValueAsString("ARCHIVED_BY");
                item.Note = reader.GetValueAsString("NOTE");
                item.ShippingAmt = reader.GetValueAsDouble("SHIPPING_AMT");
                item.ShippingType = reader.GetValueAsString("SHIPPING_TYPE");
                item.TaxAmt = reader.GetValueAsDouble("TAX_AMT");
                item.TaxType = reader.GetValueAsString("TAX_TYPE");
                item.DepositPercentage = reader.GetValueAsInt32("DEPOSIT_PCT");
                item.PONumber = reader.GetValueAsString("PO_NUMBER");
                item.PricingMethod = reader.GetValueAsString("PRICING_METHOD");
                item.PricingDefault = reader.GetValueAsString("PRICING_DEFAULT");
                item.PricingType = reader.GetValueAsString("PRICING_TYPE");
                item.TaxAmount = reader.GetValueAsDouble("TAX_AMOUNT");
                item.ShippingAmount = reader.GetValueAsDouble("SHIPPING_AMOUNT");
                item.BlueprintId = reader.GetValueAsInt32("BlueprintId");
                item.DealerPricingMethod = reader.GetValueAsString("DEALER_PRICING_METHOD");
                item.DealerPricingDefault = reader.GetValueAsString("DEALER_PRICING_DEFAULT");
                item.DealerPricingType = reader.GetValueAsString("DEALER_PRICING_TYPE");
				item.DistributorName = reader.GetValueAsString("DISTRIBUTOR_NAME");
			}

            return item;
        }

        public void ConvertToOrder(string userId, long quoteId, string orderNumber)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_convert_Quote_to_Order"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Quote Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ModifiedByUserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@ModifyDate", DateTime.Now));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OrderNumber", 250, orderNumber));

                    // Execute
                    SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public long Copy(string creatorId, string newOwnerId, long fromQuoteId, string newName, string note, bool makeTemplate)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_copy_Quote"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Customer Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@CreatorId", 12, creatorId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OwnerId", 12, newOwnerId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@FromQuoteId", fromQuoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Name", 500, newName));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@Note", note));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@MakeTemplate", makeTemplate));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        long newId = command.Parameters["@NewId"].Value.ToInt64();

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
                throw;
            }
            finally
            {

            }
        }

        public long CopyQuickQuote(string userId, long fromQuoteId, string workGroupId)
        {
            // Command
            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_copy_QuickQuote"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Customer Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@FromQuoteId", fromQuoteId));
                command.Parameters.Add(SqlFactory.BuildParameterInt("@WorkgroupId", workGroupId.ToInt32()));

                // Output for object's new database ID
                command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                // Execute
                if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Capture new ID
                    long newId = command.Parameters["@NewId"].Value.ToInt64();

                    // Return
                    return newId;
                }
                else
                {
                    return 0;
                }
            }
        }

        public void AddDoorToQuote(long quoteId, long designId, string userId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_link_QuoteToDesign"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Quote Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                    // Execute
                    SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public QuickQuote CreateQuoteFromQuickQuote(long designId, string userId, long detailId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                //using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_QuoteFromQuickQuote"))
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_copy_QuoteDesign"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", 0));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DetailId", detailId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@IsQuickQuote", true));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add("@NewDesignId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add("@NewDetailId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Cheating a little here but QuickQuote has both fields we need to send back.
                        var retval = new QuickQuote();

                        // Capture new ID
                        //long newDetailId = command.Parameters["@NewDetailId"].Value.ToInt64();
                        retval.DocumentId = command.Parameters["@NewId"].Value.ToInt64();
                        retval.DesignId = command.Parameters["@NewDesignId"].Value.ToInt64().ToString();

                        // Reset
                        //IsDirty = false;

                        // Return
                        return retval;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool Delete(long id, string userId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_Quote"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DocId", id));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public int CreateUploadAttachment(string conversationID,Int64 sourceDocId, string messageId, string filePath, string fileType, string fileName)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_UploadAttachment"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // QuoteDetail Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationID));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@SourceDocId",sourceDocId,true));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, messageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filepath", 255, filePath));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filetype", 50, fileType));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@FileName", 50, fileName));


                    // Output for object's new database ID
                    command.Parameters.Add("@returnIntId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        int newId = command.Parameters["@returnIntId"].Value.ToInt32();
                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            finally
            {

            }
        }

    }
}
