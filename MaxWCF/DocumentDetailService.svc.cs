﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class DocumentDetailService : IDocumentDetailService
    {
        /// <summary>
        /// Get all doors by Blueprint Page ID
        /// </summary>
        /// <param name="blueprintPageId"></param>
        /// <returns></returns>
        public List<DocumentDetail> GetDoors(int blueprintPageId)
        {
            List<DocumentDetail> items = new List<DocumentDetail>();

            using (SqlCommand command = new SqlCommand("usp_get_DocumentDetails"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterInt("@blueprintPageId", blueprintPageId));

                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            DocumentDetail item = new DocumentDetail();

                            item.BlueprintLocationId = reader.GetValueAsInt32("BLUEPRINT_LOCATION_ID");
                            item.BlueprintPageId = reader.GetValueAsInt32("BLUEPRINT_PAGE_ID");
                            item.CartPrice = reader.GetValueAsString("cartPrice");
                            item.CartPriceBook = reader.GetValueAsInt32("cartPriceBook");
                            item.ChildId = reader.GetValueAsString("CHILD_ID");
                            item.DateUpdated = reader.GetValueAsDate("updated");
                            item.Description = reader.GetValueAsString("DESCRIPTION");
                            item.DesignBuildNumber = reader.GetValueAsInt32("DESIGN_BUILD_NUMBER");
                            item.DetailId = reader.GetValueAsInt32("DETAIL_ID");
                            item.DiscountAmount = reader.GetValueAsString("DISCOUNT_AMT");
                            item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");
                            item.DocId = reader.GetValueAsInt64("DOC_ID");
                            item.ImageUuid = reader.GetGuid("IMAGE_UUID");
                            item.IsTaxable = reader.GetValueAsBoolean("TAXABLE");
                            item.LeadTime = reader.GetValueAsString("LEAD_TIME");
                            item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                            item.ProductRevisionLevelNumber = reader.GetValueAsString("PRODUCT_REV_LEVEL_NUM");
                            item.Quantity = reader.GetValueAsString("QUANTITY");
                            item.ReferenceNumber = reader.GetValueAsString("REFERENCE_NUM");
                            item.SequenceDescription = reader.GetValueAsString("SEQ_DESC");
                            item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                            item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                            item.UnitCost = reader.GetValueAsString("UNIT_COST");
                            item.UnitPrice = reader.GetValueAsString("UNIT_PRICE");
                            item.TypeCd = reader.GetValueAsString("TYPE_CD");
                            item.UserId = reader.GetValueAsString("USER_ID");
                            item.Width = reader.GetValueAsString("Width");
                            item.Height = reader.GetValueAsString("Height");
                            item.Thickness = reader.GetValueAsString("Thickness");
                            item.Config = reader.GetValueAsString("Config");
                            item.Core = reader.GetValueAsString("Core");
                            item.ProductLine = reader.GetValueAsString("ProductLine");
                            item.DoorStyle = reader.GetValueAsString("DoorStyle");
                            item.WallDepth = reader.GetValueAsString("WallDepth");
                            item.Swing = reader.GetValueAsString("Swing");

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        /// <summary>
        /// Get all doors on all pages of a blueprint
        /// </summary>
        /// <param name="blueprintId"></param>
        /// <returns></returns>
        public List<DocumentDetail> GetAllBlueprintDoors(int blueprintId)
        {
            List<DocumentDetail> items = new List<DocumentDetail>();

            using (SqlCommand command = new SqlCommand("usp_get_BlueprintDoors"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterInt("@blueprintId", blueprintId));

                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            DocumentDetail item = new DocumentDetail();

                            item.BlueprintLocationId = reader.GetValueAsInt32("BLUEPRINT_LOCATION_ID");
                            item.BlueprintPageId = reader.GetValueAsInt32("BLUEPRINT_PAGE_ID");
                            item.CartPrice = reader.GetValueAsString("cartPrice");
                            item.CartPriceBook = reader.GetValueAsInt32("cartPriceBook");
                            item.ChildId = reader.GetValueAsString("CHILD_ID");
                            item.DateUpdated = reader.GetValueAsDate("updated");
                            item.Description = reader.GetValueAsString("DESCRIPTION");
                            item.DesignBuildNumber = reader.GetValueAsInt32("DESIGN_BUILD_NUMBER");
                            item.DetailId = reader.GetValueAsInt32("DETAIL_ID");
                            item.DiscountAmount = reader.GetValueAsString("DISCOUNT_AMT");
                            item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");
                            item.DocId = reader.GetValueAsInt64("DOC_ID");
                            item.ImageUuid = reader.GetGuid("IMAGE_UUID");
                            item.IsTaxable = reader.GetValueAsBoolean("TAXABLE");
                            item.LeadTime = reader.GetValueAsString("LEAD_TIME");
                            item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                            item.ProductRevisionLevelNumber = reader.GetValueAsString("PRODUCT_REV_LEVEL_NUM");
                            item.Quantity = reader.GetValueAsString("QUANTITY");
                            item.ReferenceNumber = reader.GetValueAsString("REFERENCE_NUM");
                            item.SequenceDescription = reader.GetValueAsString("SEQ_DESC");
                            item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                            item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                            item.UnitCost = reader.GetValueAsString("UNIT_COST");
                            item.UnitPrice = reader.GetValueAsString("UNIT_PRICE");
                            item.TypeCd = reader.GetValueAsString("TYPE_CD");
                            item.UserId = reader.GetValueAsString("USER_ID");
                            item.Width = reader.GetValueAsString("Width");
                            item.Height = reader.GetValueAsString("Height");
                            item.Thickness = reader.GetValueAsString("Thickness");
                            item.Config = reader.GetValueAsString("Config");
                            item.Core = reader.GetValueAsString("Core");
                            item.ProductLine = reader.GetValueAsString("ProductLine");
                            item.DoorStyle = reader.GetValueAsString("DoorStyle");
                            item.WallDepth = reader.GetValueAsString("WallDepth");
                            item.Swing = reader.GetValueAsString("Swing");

                            items.Add(item);
                        }
                    }
                }
            }

            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blueprintPageId"></param>
        /// <param name="statusId"></param>
        /// <returns></returns>
        public bool UpdateBlueprintDoorStatus(int blueprintPageId, int statusId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_BlueprintDoorStatus"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintPageId", blueprintPageId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@StatusId", statusId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            finally
            {

            }
        }

        /// <summary>
        /// Get single door by item location ID on blueprint page
        /// </summary>
        /// <param name="bpLocationId"></param>
        /// <returns></returns>
        public DocumentDetail GetDoorSpec(int bpLocationId)
        {
            DocumentDetail item = new DocumentDetail();

            using(SqlCommand command = new SqlCommand("usp_get_DocumentDetail"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterInt("@bpLocId", bpLocationId));

                using(SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    if(reader != null)
                    {
                        while(reader.Read())
                        {
                            item.BlueprintLocationId = reader.GetValueAsInt32("BLUEPRINT_LOCATION_ID");
                            item.BlueprintPageId = reader.GetValueAsInt32("BLUEPRINT_PAGE_ID");
                            item.CartPrice = reader.GetValueAsString("cartPrice");
                            item.CartPriceBook = reader.GetValueAsInt32("cartPriceBook");
                            item.ChildId = reader.GetValueAsString("CHILD_ID");
                            item.DateUpdated = reader.GetValueAsDate("updated");
                            item.Description = reader.GetValueAsString("DESCRIPTION");
                            item.DesignBuildNumber = reader.GetValueAsInt32("DESIGN_BUILD_NUMBER");
                            item.DetailId = reader.GetValueAsInt32("DETAIL_ID");
                            item.DiscountAmount = reader.GetValueAsString("DISCOUNT_AMT");
                            item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");
                            item.DocId = reader.GetValueAsInt64("DOC_ID");
                            item.ImageUuid = reader.GetGuid("IMAGE_UUID");
                            item.IsTaxable = reader.GetValueAsBoolean("TAXABLE");
                            item.LeadTime = reader.GetValueAsString("LEAD_TIME");
                            item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                            item.ProductRevisionLevelNumber = reader.GetValueAsString("PRODUCT_REV_LEVEL_NUM");
                            item.Quantity = reader.GetValueAsString("QUANTITY");
                            item.ReferenceNumber = reader.GetValueAsString("REFERENCE_NUM");
                            item.SequenceDescription = reader.GetValueAsString("SEQ_DESC");
                            item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                            item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                            item.UnitCost = reader.GetValueAsString("UNIT_COST");
                            item.UnitPrice = reader.GetValueAsString("UNIT_PRICE");
                            item.TypeCd = reader.GetValueAsString("TYPE_CD");
                            item.UserId = reader.GetValueAsString("USER_ID");
                        }
                    }
                }
            }

            return item;
        }
    }
}
