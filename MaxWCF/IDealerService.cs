﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    [ServiceContract]
    public interface IDealerService
    {
        [OperationContract]
        bool Update(int masoniteId, string image);
    }

    [DataContract]
    public class Dealer
    {
        public int ConfigureOneWorkgroupId { get; set; }  // Configure One WorkGroup_Id
        public int MerchantDistributorDealerId { get; set; }
        public string MerchantDealerId { get; set; } // ConfigureOne Masonite_Id
        public int MerchantDistributorId { get; set; }
        public string Image { get; set; }
    }
}
