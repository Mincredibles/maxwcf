﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFileLocationService" in both code and config file together.
    [ServiceContract]
    public interface IFileLocationService
    {
        [OperationContract]
        FileLocation GetFileLocation(Guid uuid);

        [OperationContract]
        void Save(FileLocation fileLoc, string userId);
    }

    [DataContract]
    public class FileLocation
    {
        [DataMember]
        public Guid UUID { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string ModifiedBy { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }
    }
}
