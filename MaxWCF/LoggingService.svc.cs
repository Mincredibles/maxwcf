﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Masonite.MTier.Logging;

namespace MaxWCF
{
    public class LoggingService : ILoggingService
    {
        public bool LogByText(string text)
        {
            return LoggingHelper.Log(text);
        }

        public bool LogByApplicationMessage(string applicationName, string message, EventCategory eventCategory)
        {
            return LoggingHelper.Log(applicationName, message, eventCategory);
        }

        public bool LogByApplicationException(string applicationName, byte[] serializedException, EventCategory eventCategory)
        {
            Exception exception;
            using (var stream = new MemoryStream(serializedException))
            {
                var serializer = new NetDataContractSerializer();
                exception = serializer.Deserialize(stream) as Exception;
            }
            return LoggingHelper.Log(applicationName, exception, eventCategory);
        }
    }
}
