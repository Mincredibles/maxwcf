﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class SKUBasedItemService : ISKUBasedItemService
    {
        public List<SKUBasedItem> GetList(string userId)
        {
            List<SKUBasedItem> items = new List<SKUBasedItem>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_SKUBasedItems"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            SKUBasedItem item = new SKUBasedItem();

                            item.ItemNumber = reader.GetValueAsString("ItemNumber");
                            item.ItemDescription = reader.GetValueAsString("ItemDescription");
                            item.ItemLongDescription = reader.GetValueAsString("ItemLongDescription");
                            item.ItemExtDesc1 = reader.GetValueAsString("ItemExtDesc1");
                            item.ItemExtDesc2 = reader.GetValueAsString("ItemExtDesc2");
                            item.ItemExtDesc3 = reader.GetValueAsString("ItemExtDesc3");
                            item.ItemUOM = reader.GetValueAsString("ItemUOM");
                            item.ItemSequence = reader.GetValueAsInt32("ItemSequence");
                            item.ItemImage = reader.GetValueAsString("ItemImage");
                            item.ItemPrice = reader.GetValueAsDouble("ItemPrice");
                            item.ItemTaxable = reader.GetValueAsBoolean("ItemTaxable");
                            item.ItemLeadTime = reader.GetValueAsInt32("ItemLeadTime");
                            item.ChildCategoryId = reader.GetValueAsInt32("ChildCategoryId");
                            item.ChildCategoryDescription = reader.GetValueAsString("ChildCategoryDescription");
                            item.ChildCategorySequence = reader.GetValueAsInt32("ChildCategorySequence");
                            item.ChildCategoryImage = reader.GetValueAsString("ChildCategoryImage");
                            item.ParentCategoryId = reader.GetValueAsInt32("ParentCategoryId");
                            item.ParentCategoryDescription = reader.GetValueAsString("ParentCategoryDescription");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }
    }
}
