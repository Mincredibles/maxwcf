﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;
using System.Configuration;

namespace MaxWCF
{
    public class BlueprintService : IBlueprintService
    {
        public List<Blueprint> GetList(string userId)
        {
            List<Blueprint> items = new List<Blueprint>();

            using (SqlCommand command = new SqlCommand("usp_get_Blueprints"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Blueprint item = new Blueprint();
                            item.Scale = new BlueprintScaleType();

                            item.BlueprintId = reader.GetValueAsInt32("Id");
                            item.Name = reader.GetValueAsString("Name");
                            item.BlueprintStatus = reader.GetValueAsBoolean("BlueprintStatus");
                            item.DateCreated = reader.GetValueAsDate("DateCreated");
                            item.CreatedBy = reader.GetValueAsInt32("CreatedBy");
                            item.DateModified = reader.GetValueAsDate("DateModified");
                            item.ModifiedBy = reader.GetValueAsInt32("ModifiedBy");
                            item.WorkgroupId = reader.GetValueAsInt32("WorkgroupId");
                            item.TakeOffType = reader.GetValueAsString("TakeOffType");
                            item.BuilderName = reader.GetValueAsString("BuilderName");
                            item.Version = reader.GetValueAsString("Version");
                            item.DateCreatedReceivedFromBuilder = reader.GetValueAsDate("DateCreatedReceivedFromBuilder");
                            item.SubmittalDueDate = reader.GetValueAsDate("SubmittalDueDate");
                            item.CustomerId = reader.GetValueAsInt64("CustomerId");
                            item.ImageUUID = reader.GetGuid("ImageUUID");
                            item.PagesToAnalyze = reader.GetValueAsString("PagesToAnalyze");
                            item.QuoteId = reader.GetValueAsInt64("QuoteId");
                            item.JobSiteAddressId = reader.GetValueAsInt64("JobSiteAddressId");
                            item.Scale.Id = reader.GetValueAsInt32("ScaleTypeId");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public Blueprint Get(int blueprintId)
        {
            Blueprint item = new Blueprint();

            using (SqlCommand command = new SqlCommand("usp_get_Blueprint"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintId", blueprintId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();
                        item.Scale = new BlueprintScaleType();

                        item.BlueprintId = reader.GetValueAsInt32("Id");
                        item.Name = reader.GetValueAsString("Name");
                        item.BlueprintStatus = reader.GetValueAsBoolean("BlueprintStatus");
                        item.DateCreated = reader.GetValueAsDate("DateCreated");
                        item.CreatedBy = reader.GetValueAsInt32("CreatedBy");
                        item.DateModified = reader.GetValueAsDate("DateModified");
                        item.ModifiedBy = reader.GetValueAsInt32("ModifiedBy");
                        item.WorkgroupId = reader.GetValueAsInt32("WorkgroupId");
                        item.TakeOffType = reader.GetValueAsString("TakeOffType");
                        item.BuilderName = reader.GetValueAsString("BuilderName");
                        item.Version = reader.GetValueAsString("Version");
                        item.DateCreatedReceivedFromBuilder = reader.GetValueAsDate("DateCreatedReceivedFromBuilder");
                        item.SubmittalDueDate = reader.GetValueAsDate("SubmittalDueDate");
                        item.CustomerId = reader.GetValueAsInt64("CustomerId");
                        item.CustomerName = reader.GetValueAsString("CustomerName");
                        item.ImageUUID = reader.GetGuid("ImageUUID");
                        item.PagesToAnalyze = reader.GetValueAsString("PagesToAnalyze");
                        item.QuoteId = reader.GetValueAsInt64("QuoteId");
                        item.JobSiteAddressId = reader.GetValueAsInt64("JobSiteAddressId");
                        item.JobAddress.AddressLine1 = reader.GetValueAsString("JobAddressLine1");
                        item.JobAddress.AddressLine2 = reader.GetValueAsString("JobAddressLine2");
                        item.JobAddress.City = reader.GetValueAsString("JobCity");
                        item.JobAddress.State = reader.GetValueAsString("JobState");
                        item.JobAddress.Country = reader.GetValueAsString("JobCountry");
                        item.JobAddress.PostalCode = reader.GetValueAsString("JobPostalCode");
                        item.Scale.Id = reader.GetValueAsInt32("ScaleTypeId");
                        item.Scale.Name = reader.GetValueAsString("ScaleTypeName");
    }
                }
            }

            return item;
        }

        public int Update(Blueprint blueprint, int userId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_Blueprint"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Blueprint Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintId", blueprint.BlueprintId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Name", 200, blueprint.Name));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@BlueprintStatus", blueprint.BlueprintStatus));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@WorkgroupId", blueprint.WorkgroupId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@UserId", userId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@TakeOffType", 150, blueprint.TakeOffType));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@BuilderName", 150, blueprint.BuilderName));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Version", 50, blueprint.Version));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@DateCreatedReceivedFromBuilder", blueprint.DateCreatedReceivedFromBuilder));
                    command.Parameters.Add(SqlFactory.BuildParameterDateTime("@SubmittalDueDate", blueprint.SubmittalDueDate));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerId", blueprint.CustomerId));
                    command.Parameters.Add(SqlFactory.BuildParameterGuid("@imageUUID", blueprint.ImageUUID));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@PagesToAnalyze", 1000, blueprint.PagesToAnalyze));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@JobSiteAddressId", blueprint.JobSiteAddressId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@ScaleTypeId", blueprint.Scale.Id));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        int newId = command.Parameters["@NewId"].Value.ToInt32();
                        if (newId > 0) { blueprint.BlueprintId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public bool Delete(int blueprintId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_Blueprint"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@BlueprintId", blueprintId));

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Reset
                        //IsActive = false;
                        //IsDirty = false;

                        // Return
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public List<BlueprintScaleType> GetScaleTypes()
        {
            List<BlueprintScaleType> items = new List<BlueprintScaleType>();

            using (SqlCommand command = new SqlCommand("usp_get_BlueprintScaleTypes"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            BlueprintScaleType item = new BlueprintScaleType();

                            item.Id = reader.GetValueAsInt32("Id");
                            item.Name = reader.GetValueAsString("Name");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public List<Blueprint> GetAllBlueprints()
        {
            List<Blueprint> items = new List<Blueprint>();

            using (SqlCommand command = new SqlCommand("usp_get_AllBlueprints"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;
                
                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            Blueprint item = new Blueprint();
                            item.Scale = new BlueprintScaleType();

                            item.BlueprintId = reader.GetValueAsInt32("Id");
                            item.Name = reader.GetValueAsString("Name");
                            item.BlueprintStatus = reader.GetValueAsBoolean("BlueprintStatus");
                            item.DateCreated = reader.GetValueAsDate("DateCreated");
                            item.CreatedBy = reader.GetValueAsInt32("CreatedBy");
                            item.DateModified = reader.GetValueAsDate("DateModified");
                            item.ModifiedBy = reader.GetValueAsInt32("ModifiedBy");
                            item.WorkgroupId = reader.GetValueAsInt32("WorkgroupId");
                            item.TakeOffType = reader.GetValueAsString("TakeOffType");
                            item.BuilderName = reader.GetValueAsString("BuilderName");
                            item.Version = reader.GetValueAsString("Version");
                            item.DateCreatedReceivedFromBuilder = reader.GetValueAsDate("DateCreatedReceivedFromBuilder");
                            item.SubmittalDueDate = reader.GetValueAsDate("SubmittalDueDate");
                            item.CustomerId = reader.GetValueAsInt64("CustomerId");
                            item.ImageUUID = reader.GetGuid("ImageUUID");
                            item.PagesToAnalyze = reader.GetValueAsString("PagesToAnalyze");
                            item.QuoteId = reader.GetValueAsInt64("QuoteId");
                            item.JobSiteAddressId = reader.GetValueAsInt64("JobSiteAddressId");
                            item.Scale.Id = reader.GetValueAsInt32("ScaleTypeId");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }
    }
}
