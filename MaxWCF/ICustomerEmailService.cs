﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface ICustomerEmailService
    {
        [OperationContract]
        CustomerEmail Get(string messageId);

        [OperationContract]
        List<CustomerEmail> GetCustomerEmails(string userId);

        [OperationContract]
        List<CustomerEmail> GetConversationMessages(string conversationId);

        [OperationContract]
        List<MessageAttachment> GetConversationMessageAttachments(string conversationId, string messageId);

        [OperationContract]
        List<MessageAttachment> GetQuoteAttachments(long quoteId);

        [OperationContract]
        bool InsertConversationMessageAttachment(string conversationId, string messageId, string filePath, string fileName, string fileType);

        [OperationContract]
        bool InsertConversationMessage(CustomerEmail conversationMessage, string userId);

        [OperationContract]
        bool DeleteCustomerEmail(string messageId);
    }

    [DataContract]
    public class CustomerEmail
    {
        [DataMember]
        public string ConversationId { get; set; }

        [DataMember]
        public string MessageId { get; set; }
        
        [DataMember]
        public string Body { get; set; }
        
        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public long MessageCustomerId { get; set; }

        [DataMember]
        public string MessageOwner { get; set; }

        [DataMember]
        public string MessageStatus { get; set; }

        [DataMember]
        public string Subject { get; set; }

        [DataMember]
        public string MessageType { get; set; }

        [DataMember]
        public DateTime ConversationCreateDate { get; set; }

        [DataMember]
        public long SourceDocumentId { get; set; }

        [DataMember]
        public string SourceTypeCode { get; set; }

        [DataMember]
        public string RecipientName { get; set; }

        [DataMember]
        public string OwnerFirstName { get; set; }

        [DataMember]
        public string OwnerMiddleInitial { get; set; }

        [DataMember]
        public string OwnerLastName { get; set; }

        [DataMember]
        public string OwnerEmail { get; set; }

        [DataMember]
        public string RecipientId { get; set; }

        [DataMember]
        public string RecipientEmailAddress { get; set; }

        [DataMember]
        public string RecipientStatus { get; set; }

        [DataMember]
        public long RecipientUserCustomerId { get; set; }

        [DataMember]
        public string RecipientUserId { get; set; }

        [DataMember]
        public string QuoteName { get; set; }
		[DataMember]
		public string QuoteSerialNumber { get; set; }
    }


    [DataContract]
    public class MessageAttachment
    {
        [DataMember]
        public string ConversationId { get; set; }

        [DataMember]
        public string MessageId { get; set; }

        [DataMember]
        public string FilePath { get; set; }

        [DataMember]
        public string FileType { get; set; }

        [DataMember]
        public string FileName { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }
    }
}
