﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class DocumentHistoryService : IDocumentHistoryService
    {
        public List<DocumentHistory> GetDocumentHistory(long documentId)
        {
            List<DocumentHistory> items = new List<DocumentHistory>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_DocumentHistory"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DocumentId", documentId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            DocumentHistory item = new DocumentHistory();

                            item.UserId = reader.GetValueAsString("USER_ID");
                            item.DocumentId = reader.GetValueAsInt64("DOCUMENT_ID");
                            item.EventDate = reader.GetValueAsDate("EVENT_DATE");
                            item.EventCode = reader.GetValueAsString("EVENT_CODE");
                            item.EventUserId = reader.GetValueAsString("EVENT_USER_ID");
                            item.EventDescription = reader.GetValueAsString("EVENT_DESCRIPTION");
                            item.UserFirstName = reader.GetValueAsString("USER_FIRST_NAME");
                            item.UserLastName = reader.GetValueAsString("USER_LAST_NAME");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public bool InsertDocumentHistory(DocumentHistory customerHistory)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_DocumentHistory"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Document Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, customerHistory.UserId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DocumentId", customerHistory.DocumentId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@EventCode", 20, customerHistory.EventCode));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@EventUserId", 20, customerHistory.EventUserId));
                    command.Parameters.Add(SqlFactory.BuildParameterNText("@EventDescription", customerHistory.EventDescription));
                                                                                                              
                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
