﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class QuoteDetailService : IQuoteDetailService
    {
        public List<QuoteDetail> GetList(long quoteId)
        {
            List<QuoteDetail> items = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_QuoteDetails"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    items = GetDetailsFromReader(reader);
            }
            return items;
        }

        private List<QuoteDetail> GetDetailsFromReader(SqlDataReader reader)
        {
            List<QuoteDetail> items = new List<QuoteDetail>();

            // Populate local object
            if (reader != null)
            {
                while (reader.Read())
                {
                    QuoteDetail item = new QuoteDetail();

                    item.UserId = reader.GetValueAsString("USER_ID");
                    item.QuoteId = reader.GetValueAsInt64("DOC_ID");
                    item.QuoteDetailId = reader.GetValueAsInt32("DETAIL_ID");
                    item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                    item.SequenceDescription = reader.GetValueAsString("SEQ_DESC");
                    item.TypeCode = reader.GetValueAsString("TYPE_CD");
                    item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                    item.Quantity = reader.GetValueAsString("QUANTITY");
                    item.ProductRevisionLevelNumber = reader.GetValueAsString("PRODUCT_REV_LEVEL_NUM");
                    item.ChildId = reader.GetValueAsString("CHILD_ID");
                    item.DesignBuildNumber = reader.GetValueAsInt32("DESIGN_BUILD_NUMBER");
                    item.DiscountAmt = reader.GetValueAsString("DISCOUNT_AMT");
                    item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");
                    item.Description = reader.GetValueAsString("DESCRIPTION");
                    item.UnitPrice = reader.GetValueAsDouble("UNIT_PRICE");
                    item.UnitCost = reader.GetValueAsDouble("UNIT_COST");
                    item.ReferenceNumber = reader.GetValueAsString("REFERENCE_NUM");
                    item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                    item.Taxable = reader.GetValueAsBoolean("TAXABLE");
                    item.ApplyDiscount = reader.GetValueAsBoolean("APPLY_PRICING_METHOD");
                    item.IncludeMiscOnPurchaseOrder = reader.GetValueAsBoolean("INCLUDE_MISC_ON_PURCHASE_ORDER");
                    item.LeadTime = reader.GetValueAsString("LEAD_TIME");
                    item.UserPrice = reader.GetValueAsString("USER_PRICE");
                    item.ProductId = reader.GetValueAsInt32("PRODUCT_ID");
                    item.ImageFileName = reader.GetValueAsString("file_name");
                    item.Comment = reader.GetValueAsString("Comment");
                    item.DoorLocation = reader.GetValueAsString("DoorLocation");
                    item.IsNetUnit = reader.GetValueAsString("IsNetUnit");
                    item.FileId = reader.GetValueAsString("IMAGE_UUID");
                    item.FileLocation = reader.GetValueAsString("FileLocation");
                    item.FileCreatedBy.UserId = reader.GetValueAsString("FileCreatedBy");
                    item.FileCreatedBy.UserName = reader.GetValueAsString("FileCreatedUserName");
                    item.FileCreatedBy.FirstName = reader.GetValueAsString("FileCreatedFirstName");
                    item.FileCreatedBy.MiddleInitial = reader.GetValueAsString("FileCreatedMiddleInitial");
                    item.FileCreatedBy.LastName = reader.GetValueAsString("FileCreatedLastName");
                    item.FileCreatedBy.CompanyName = reader.GetValueAsString("FileCreatedCompanyName");

                    // Add to collection
                    items.Add(item);
                }
            }

            return items;
        }

        public QuoteDetail Get(int quoteDetailId)
        {
            QuoteDetail item = new QuoteDetail();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_QuoteDetail"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterInt("@QuoteDetailId", quoteDetailId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if ((reader != null) && reader.Read())
                    {
                        item.UserId = reader.GetValueAsString("USER_ID");
                        item.QuoteId = reader.GetValueAsInt64("DOC_ID");
                        item.QuoteDetailId = reader.GetValueAsInt32("DETAIL_ID");
                        item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                        item.SequenceDescription = reader.GetValueAsString("SEQ_DESC");
                        item.TypeCode = reader.GetValueAsString("TYPE_CD");
                        item.TemplateId = reader.GetValueAsInt32("TEMPLATE_ID");
                        item.Quantity = reader.GetValueAsString("QUANTITY");
                        item.ProductRevisionLevelNumber = reader.GetValueAsString("PRODUCT_REV_LEVEL_NUM");
                        item.ChildId = reader.GetValueAsString("CHILD_ID");
                        item.DesignBuildNumber = reader.GetValueAsInt32("DESIGN_BUILD_NUMBER");
                        item.DiscountAmt = reader.GetValueAsString("DISCOUNT_AMT");
                        item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");
                        item.Description = reader.GetValueAsString("DESCRIPTION");
                        item.UnitPrice = reader.GetValueAsDouble("UNIT_PRICE");
                        item.UnitCost = reader.GetValueAsDouble("UNIT_COST");
                        item.ReferenceNumber = reader.GetValueAsString("REFERENCE_NUM");
                        item.PriceBookId = reader.GetValueAsInt32("PRICE_BOOK_ID");
                        item.Taxable = reader.GetValueAsBoolean("TAXABLE");
                        item.ApplyDiscount = reader.GetValueAsBoolean("APPLY_PRICING_METHOD");
                        item.IncludeMiscOnPurchaseOrder = reader.GetValueAsBoolean("INCLUDE_MISC_ON_PURCHASE_ORDER");
                        item.LeadTime = reader.GetValueAsString("LEAD_TIME");
                        item.UserPrice = reader.GetValueAsString("USER_PRICE");
                        item.ProductId = reader.GetValueAsInt32("PRODUCT_ID");
                        item.ImageFileName = reader.GetValueAsString("file_name");
                        item.Comment = reader.GetValueAsString("Comment");
                        item.DoorLocation = reader.GetValueAsString("DoorLocation");
                        item.IsNetUnit = reader.GetValueAsString("IsNetUnit");
                        item.FileId = reader.GetValueAsString("IMAGE_UUID");
                        item.FileLocation = reader.GetValueAsString("FileLocation");
                        item.FileCreatedBy.UserId = reader.GetValueAsString("FileCreatedBy");
                        item.FileCreatedBy.UserName = reader.GetValueAsString("FileCreatedUserName");
                        item.FileCreatedBy.FirstName = reader.GetValueAsString("FileCreatedFirstName");
                        item.FileCreatedBy.MiddleInitial = reader.GetValueAsString("FileCreatedMiddleInitial");
                        item.FileCreatedBy.LastName = reader.GetValueAsString("FileCreatedLastName");
                        item.FileCreatedBy.CompanyName = reader.GetValueAsString("FileCreatedCompanyName");
                    }
                }
            }
            return item;
        }

        public int Update(string userId, QuoteDetail quoteDetail)
        {
            return Update(userId, quoteDetail, true);
        }

        private int Update(string userId, QuoteDetail quoteDetail, bool applyDiscount)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_update_QuoteDetail"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // QuoteDetail Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 20, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@OwnerUserId", 20, quoteDetail.UserId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteDetail.QuoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@QuoteDetailId", quoteDetail.QuoteDetailId));
                    command.Parameters.Add(SqlFactory.BuildParameterSmallInt("@SequenceNumber", quoteDetail.SequenceNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@SequenceDescription", 10, quoteDetail.SequenceDescription));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@TypeCode", 2, quoteDetail.TypeCode));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@TemplateId", quoteDetail.TemplateId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Quantity", 50, quoteDetail.Quantity));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@ProductRevisionLevelNumber", 2, quoteDetail.ProductRevisionLevelNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ChildId", 250, quoteDetail.ChildId));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@DesignBuildNumber", quoteDetail.DesignBuildNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@DiscountAmt", 50, string.IsNullOrEmpty(quoteDetail.DiscountAmt) ? "0" : quoteDetail.DiscountAmt));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@DiscountType", 1, quoteDetail.DiscountType));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Description", 500, quoteDetail.Description));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@UnitPrice", quoteDetail.UnitPrice));
                    command.Parameters.Add(SqlFactory.BuildParameterFloat("@UnitCost", quoteDetail.UnitCost));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ReferenceNumber", 80, quoteDetail.ReferenceNumber));
                    command.Parameters.Add(SqlFactory.BuildParameterInt("@PriceBookId", quoteDetail.PriceBookId));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@Taxable", quoteDetail.Taxable));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@LeadTime", 50, quoteDetail.LeadTime));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserPrice", 50, quoteDetail.UserPrice));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@Comment", 4000, quoteDetail.Comment));
                    command.Parameters.Add(SqlFactory.BuildParameterNVarChar("@DoorLocation", 4000, quoteDetail.DoorLocation));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@ApplyDiscount", applyDiscount));
                    command.Parameters.Add(SqlFactory.BuildParameterBit("@IncludeMiscOnPurchaseOrder", quoteDetail.IncludeMiscOnPurchaseOrder));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        int newId = command.Parameters["@NewId"].Value.ToInt32();
                        if (newId > 0) { quoteDetail.QuoteDetailId = newId; }

                        // Reset
                        //IsDirty = false;

                        // Return
                        return newId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }

        public QuoteDetail InsertSKUBasedItem(QuoteDetail quoteDetail, bool applyDiscount)
        {
            if (!applyDiscount)
            {
                quoteDetail.DiscountAmt = "0";
            }
            // Proc sets some values so we'll save and then load.
            var newId = Update(quoteDetail.UserId, quoteDetail, applyDiscount);
            return Get(newId);
        }

        public List<QuoteDetail> ReorderDetails(long quoteId, int sourceQuoteDetailId, int destinationQuoteDetailId)
        {
            List<QuoteDetail> items = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_reorder_QuoteDetails"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));
                command.Parameters.Add(SqlFactory.BuildParameterInt("@SourceQuoteDetailId", sourceQuoteDetailId));
                command.Parameters.Add(SqlFactory.BuildParameterInt("@DestinationQuoteDetailId", destinationQuoteDetailId));

                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    items = GetDetailsFromReader(reader);
            }
            return items;
        }

        public List<QuoteDetailItem> GetQuoteDetailItemList(long quoteId, long detailId)
        {
            List<QuoteDetailItem> items = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_QuoteDetailItems"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DetailId", detailId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    items = GetDetailItemsFromReader(reader);
            }
            return items;
        }

        private List<QuoteDetailItem> GetDetailItemsFromReader(SqlDataReader reader)
        {
            List<QuoteDetailItem> items = new List<QuoteDetailItem>();

            // Populate local object
            if (reader != null)
            {
                while (reader.Read())
                {
                    QuoteDetailItem item = new QuoteDetailItem();

                    item.UserId = reader.GetValueAsString("USER_ID");
                    item.QuoteId = reader.GetValueAsInt64("DOC_ID");
                    item.QuoteDetailId = reader.GetValueAsInt32("DETAIL_ID");
                    item.SequenceNumber = reader.GetValueAsInt16("SEQ_NUM");
                    item.SmartpartNumber = reader.GetValueAsString("SMARTPART_NUM");
                    item.ItemNumber = reader.GetValueAsString("ITEM_NUM");
                    item.Description = reader.GetValueAsString("DESCRIPTION");
                    item.DespartId = reader.GetValueAsString("DESPART_ID");
                    item.DespartParentId = reader.GetValueAsString("DESPART_PARENT_ID");
                    item.UnitPrice = reader.GetValueAsString("UNIT_PRICE");
                    item.UnitCost = reader.GetValueAsString("UNIT_COST");
                    item.Quantity = reader.GetValueAsInt32("QUANTITY");
                    item.DiscountAmount = reader.GetValueAsString("DISCOUNT_AMT");
                    item.DiscountType = reader.GetValueAsString("DISCOUNT_TYPE");

                    // Add to collection
                    items.Add(item);
                }
            }

            return items;
        }

        public string GetWarranty(long designId)
        {
            string item = "";

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_DoorWarranty"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if ((reader != null) && reader.Read())
                    {
                        item = reader.GetValueAsString("Sub_Family_Warranty_Link");
                    }
                }
            }
            return item;
        }

        public List<QuoteDetail> Delete(long docId, long detailId, string userId)
        {
            List<QuoteDetail> items = null;
            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_QuoteDetail"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", docId));
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteDetailId", detailId));
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    items = GetDetailsFromReader(reader);
            }
            return items;
        }

        public List<QuoteDesignItem> GetDesignItemList(string userId, long designId)
        {
            List<QuoteDesignItem> items = null;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_QuoteDesignItems"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignID", designId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    items = GetDesignItemsFromReader(reader);
            }
            return items;
        }

        private List<QuoteDesignItem> GetDesignItemsFromReader(SqlDataReader reader)
        {
            List<QuoteDesignItem> items = new List<QuoteDesignItem>();

            // Populate local object
            if (reader != null)
            {
                while (reader.Read())
                {
                    QuoteDesignItem item = new QuoteDesignItem();

                    item.ProductId = reader.GetValueAsInt32("PRODUCT_ID");
                    item.UserId = reader.GetValueAsString("USER_ID");
                    item.DesignId = reader.GetValueAsInt64("DESIGN_ID");
                    item.ProductInputNum = reader.GetValueAsInt32("PRODUCT_INPUT_NUM");
                    item.InputName = reader.GetValueAsString("INPUT_NAME");
                    item.InputTypeCd = reader.GetValueAsString("INPUT_TYPE_CD");
                    item.DesignInputVal = reader.GetValueAsString("DESIGN_INPUT_VAL");
                    item.InputLabel = reader.GetValueAsString("INPUT_LABEL");
                    item.LogicId = reader.GetValueAsInt32("LOGIC_ID");
                    item.SeqNum = reader.GetValueAsInt32("SEQ_NUM");
                    item.Price = reader.GetValueAsString("PRICE");
                    item.Opt = reader.GetValueAsString("opt");
                    item.Updated = reader.GetValueAsDate("updated");
                    item.ValueDesc = reader.GetValueAsString("VALUE_DESC");

                    // Add to collection
                    items.Add(item);
                }
            }

            return items;
        }

        public long CopyQuoteDetail(long designId, string userId, long quoteId, long detailId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_copy_QuoteDesign"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DesignId", designId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@DetailId", detailId));

                    // Output for object's new database ID
                    command.Parameters.Add("@NewId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add("@NewDesignId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add("@NewDetailId", System.Data.SqlDbType.BigInt).Direction = System.Data.ParameterDirection.Output;

                    // Execute
                    if (SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Capture new ID
                        long newDetailId = command.Parameters["@NewDetailId"].Value.ToInt64();
                        // Return
                        return newDetailId;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return 0;
            }
        }
    }
}
