﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class MerchantContactService : IMerchantContactService
    {
        /// <summary>
        /// Retrieves a single MerchantContact, using the provided email address.
        /// </summary>
        /// <param name="masoniteDotComLogon">The email address for the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        public MerchantContact RetrieveMerchantContactByEmail(string emailAddress)
        {
            var salesPointContact = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContact(emailAddress);

            return ConvertFromSalesPointContact(salesPointContact);
        }

        /// <summary>
        /// Retrieves a single MerchantContact, using the provided ID.
        /// </summary>
        /// <param name="merchantContactId">The ID of the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        public MerchantContact RetrieveMerchantContactById(int merchantContactId)
        {
            var salesPointContact = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContact(merchantContactId);

            return ConvertFromSalesPointContact(salesPointContact);
        }

        /// <summary>
        /// Retrieves a single MerchantContact, using the provided logon/password pair.
        /// </summary>
        /// <param name="masoniteDotComLogon">The login name for the MerchantContact to retrieve.</param>
        /// <param name="masoniteDotComPassword">The password for the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        public MerchantContact RetrieveMerchantContactByUserNamePassword(string masoniteDotComLogon, string masoniteDotComPassword)
        {
            var salesPointContact = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContact(masoniteDotComLogon, masoniteDotComPassword);

            return ConvertFromSalesPointContact(salesPointContact);
        }

        private MerchantContact ConvertFromSalesPointContact(Masonite.MTier.SalesPoint.MerchantContact salesPointContact)
        {
            return new MerchantContact
            {
                Id = salesPointContact.Id,
                FirstName = salesPointContact.FirstName,
                LastName = salesPointContact.LastName,
                EmailAddress = salesPointContact.EmailAddress,
                IsAllowedAccessToDealerSite = salesPointContact.IsAllowedAccessToDealerSite,
                IsMasoniteDotComLogonLocked = salesPointContact.IsMasoniteDotComLogonLocked,
                IsMaxAdministrator = salesPointContact.IsMaxAdministrator
            };
        }

        /// <summary>
        /// Retrieves a generic list of the top 50 MerchantContactConfigureOne objects for a given Merchant Contact Id.
        /// </summary>
        /// <param name="merchantContactId">The ID of the contact to retrieve MerchantContactConfigureOne for.</param>
        /// <returns>A Generic List of MerchantContactConfigureOne objects.</returns>
        public List<MerchantContactConfigureOne> RetrieveMerchantContactConfigureOnes(int merchantContactId)
        {
            var contactConfigureOnes = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContactConfigureOnes(merchantContactId);

            return (from contactConfigureOne in contactConfigureOnes
                    select new MerchantContactConfigureOne
                    {
                        ConfigureOneUserId = contactConfigureOne.ConfigureOneUserId,
                        Id = contactConfigureOne.Id,
                        IsReceivingCartEmail = contactConfigureOne.IsReceivingCartEmail,
                        MerchantContactId = contactConfigureOne.MerchantContactId,
                        MerchantDistributorDealerId = contactConfigureOne.MerchantDistributorDealerId,
                        MerchantDealerId = contactConfigureOne.MerchantDealerId,
                        MerchantDealerName = contactConfigureOne.MerchantDealerName,
                        MerchantDistributorId = contactConfigureOne.MerchantDistributorId,
                        ConfigureOneWorkgroupId = contactConfigureOne.ConfigureOneWorkgroupId,
                        MerchantDistributorName = contactConfigureOne.MerchantDistributorName,
                        MaxVersion = contactConfigureOne.MaxVersion,
                        MaxUrl = contactConfigureOne.MaxUrl,
                        Image = contactConfigureOne.Image,
                        CreatedBy = contactConfigureOne.CreatedBy,
                        DateCreated = contactConfigureOne.DateCreated,
                        DateModified = contactConfigureOne.DateModified,
                        ModifiedBy = contactConfigureOne.ModifiedBy
                    }).ToList();
        }

        /// <summary>
        /// Retrieves Permissions
        /// </summary>
        public List<string> RetrieveMerchantContactConfigureOnesPermissions(int configureOneUserId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContactConfigureOnesPermissions(configureOneUserId);
        }

        public Dictionary<int, List<Masonite.MTier.SalesPoint.MerchantDistributorDealerUsersPermissions>> GetUserPermissionList(int configureOneId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantDistributorDealerUsersPermissions(configureOneId);
        }

        public string RetrieveConfiguratorSignOnUrl(int configureOneId)
        {
            var mTierConfigureOne = Masonite.MTier.SalesPoint.ObjectFactory.RetrieveMerchantContactConfigureOne(configureOneId);

            return Masonite.MTier.Configurator.HelperFunctions.RetrieveConfiguratorSignOnUrl(mTierConfigureOne, null);
        }

        public List<Masonite.MTier.SalesPoint.ConfigureOneUser> RetriveUsersInSameDealer(int configureOneId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.RetriveUsersInSameDealer(configureOneId);
        }

        public List<Masonite.MTier.SalesPoint.MerchantDistributorDealerPermission> RetriveDefaultDealerPermission(int workgroupId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.RetriveDefaultDealerPermission(workgroupId);
        }

        public int UpdateConfigureOnePermission(int configureOneId, int permissionId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.UpdateConfigureOnePermission(configureOneId, permissionId);
        }

        public bool DeleteConfigureOnePermission(int configureOneId, int permissionId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.DeleteConfigureOnePermission(configureOneId, permissionId);
        }

        public int InsertDefaultPermission(int workgroupId, int permissionId)
        {
            return Masonite.MTier.SalesPoint.ObjectFactory.InsertDefaultDealerPermission(workgroupId, permissionId);
        }

        public string GetSupportEmail(long workgroupId)
        {
            string supportEmail = string.Empty;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_SupportEmail"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterBigInt("@WorkgroupId", workgroupId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        reader.Read();

                        supportEmail = reader.GetValueAsString("support_email");
                    }
                }
            }

            return supportEmail;
        }
    }
}
