﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Masonite.MTier.Logging;

namespace MaxWCF
{
    [ServiceContract]
    public interface ILoggingService
    {
        [OperationContract]
        bool LogByText(string text);

        [OperationContract]
        bool LogByApplicationMessage(string applicationName, string message, EventCategory eventCategory);

        [OperationContract]
        bool LogByApplicationException(string applicationName, byte[] serializedException, EventCategory eventCategory);
    }
}
