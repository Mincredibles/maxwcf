﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface ICustomerHistoryService
    {
        [OperationContract]
        List<CustomerHistory> GetCustomerHistory(long userCustomerId);

        [OperationContract]
        bool InsertCustomerHistory(CustomerHistory customerHistory);
    }

    [DataContract]
    public class CustomerHistory
    {
        public CustomerHistory()
        {
            EventUser = new User();
        }
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public long UserCustomerId { get; set; }

        [DataMember]
        public DateTime EventDate { get; set; }

        [DataMember]
        public string EventCode { get; set; }

        [DataMember]
        public User EventUser { get; set; }

        [DataMember]
        public string EventDescription { get; set; }
    }
}
