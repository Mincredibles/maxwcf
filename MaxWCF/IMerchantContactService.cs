﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IMerchantContactService
    {
        /// <summary>
        /// Retrieves a single MerchantContact, using the provided email address.
        /// </summary>
        /// <param name="masoniteDotComLogon">The email address for the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        [OperationContract]
        MerchantContact RetrieveMerchantContactByEmail(string emailAddress);

        /// <summary>
        /// Retrieves a single MerchantContact, using the provided ID.
        /// </summary>
        /// <param name="merchantContactId">The ID of the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        [OperationContract]
        MerchantContact RetrieveMerchantContactById(int merchantContactId);

        /// <summary>
        /// Retrieves a single MerchantContact, using the provided logon/password pair.
        /// </summary>
        /// <param name="masoniteDotComLogon">The login name for the MerchantContact to retrieve.</param>
        /// <param name="masoniteDotComPassword">The password for the MerchantContact to retrieve.</param>
        /// <returns>A MerchantContact object.</returns>
        [OperationContract]
        MerchantContact RetrieveMerchantContactByUserNamePassword(string masoniteDotComLogon, string masoniteDotComPassword);

        /// <summary>
        /// Retrieves a generic list of the top 50 MerchantContactConfigureOne objects for a given Merchant Contact Id.
        /// </summary>
        /// <param name="merchantContactId">The ID of the contact to retrieve MerchantContactConfigureOne for.</param>
        /// <returns>A Generic List of MerchantContactConfigureOne objects.</returns>
        [OperationContract]
        List<MerchantContactConfigureOne> RetrieveMerchantContactConfigureOnes(int merchantContactId);

        /// <summary>
        /// Retrieves Permissions
        /// </summary>
        [OperationContract]
        List<string> RetrieveMerchantContactConfigureOnesPermissions(int configureOneUserId);

        [OperationContract]
        Dictionary<int, List<Masonite.MTier.SalesPoint.MerchantDistributorDealerUsersPermissions>> GetUserPermissionList(int configureOneId);

        [OperationContract]
        List<Masonite.MTier.SalesPoint.ConfigureOneUser> RetriveUsersInSameDealer(int configureOneId);

        [OperationContract]
        int UpdateConfigureOnePermission(int configureOneId, int permissionId);

        [OperationContract]
        bool DeleteConfigureOnePermission(int configureOneId, int permissionId);

        [OperationContract]
        string RetrieveConfiguratorSignOnUrl(int configureOneId);

        [OperationContract]
        int InsertDefaultPermission(int workgroupId, int permissionId);
        
        [OperationContract]
        List<Masonite.MTier.SalesPoint.MerchantDistributorDealerPermission> RetriveDefaultDealerPermission(int workgroupId);
        
        [OperationContract]
        string GetSupportEmail(long workgroupId);
    }

    [DataContract]
    public class MerchantContact
    {
        /// <summary>
        /// Gets the object's unique ID.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the contact's first name.
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the contact's last name.
        /// </summary>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        [DataMember]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets a value that indicates whether the contact is allowed to access the dealer section of the Masonite.com website.
        /// </summary>
        [DataMember]
        public bool IsAllowedAccessToDealerSite { get; set; }

        /// <summary>
        /// Gets a value that indicates whether the MerchantContact's MasoniteDotCom logon account has been locked due to inactivity.
        /// </summary>
        /// <remarks>The current threshold for lock-out is 90 days.</remarks>
        [DataMember]
        public bool IsMasoniteDotComLogonLocked { get; set; }

        [DataMember]
        public bool IsMaxAdministrator { get; set; }
    }

    [DataContract]
    public class MerchantContactConfigureOne
    {
        /// <summary>
        /// Gets the object's unique ID.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ID of the merchant contact.
        /// </summary>
        [DataMember]
        public int MerchantContactId { get; set; }

        /// <summary>
        /// Gets the ID of the merchant contact.
        /// </summary>
        [DataMember]
        public int MerchantDistributorDealerId { get; set; }

        /// <summary>
        /// Gets the ID of the associated distributor.  This property is read-only.
        /// </summary>
        [DataMember]
        public int ConfigureOneWorkgroupId { get; set; }

        /// <summary>
        /// Gets the ID of the associated distributor.
        /// </summary>
        [DataMember]
        public int MerchantDistributorId { get; set; }

        /// <summary>
        /// Gets the name of the associated distributor.
        /// </summary>
        [DataMember]
        public string MerchantDistributorName { get; set; }

        /// <summary>
        /// Gets the ID of the associated dealer.
        /// </summary>
        [DataMember]
        public int MerchantDealerId { get; set; }

        /// <summary>
        /// Gets the name of the associated dealer.
        /// </summary>
        [DataMember]
        public string MerchantDealerName { get; set; }

        /// <summary>
        /// Gets the contact's associated ConfigureOne user id.
        /// </summary>
        [DataMember]
        public string ConfigureOneUserId { get; set; }

        /// <summary>
        /// Gets a value that indicates whether the contact prefers to receive cart emails.
        /// </summary>
        [DataMember]
        public bool IsReceivingCartEmail { get; set; }

        [DataMember]
        public string MaxVersion { get; set; }

        [DataMember]
        public string MaxUrl { get; set; }

        /// <summary>
        /// Gets or sets a value the logo for the distributor
        /// </summary>
        [DataMember]
        public string Image { get; set; }

        /// <summary>
        /// Gets the name of the user that created this object.
        /// </summary>
        [DataMember]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets the name of the user that last modified this object.
        /// </summary>
        [DataMember]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets the date that this object was created.
        /// </summary>
        [DataMember]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Gets the date that this object was last modified.
        /// </summary>
        [DataMember]
        public DateTime DateModified { get; set; }
    }
}
