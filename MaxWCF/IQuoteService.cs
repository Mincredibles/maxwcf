﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IQuoteService
    {
        [OperationContract]
        List<Quote> GetList(string userId, long customerId);

        [OperationContract]
        List<Quote> GetTemplates(string userId, long customerId);

        [OperationContract]
        List<Quote> GetMyQuotesList(string userId, long customerId);

        [OperationContract]
        List<Quote> GetQuoteListForLoggedInDealer(string userId, long customerId, bool includeWorkGroups);

        [OperationContract]
        List<QuickQuote> GetQuickQuotesForLoggedInDealer(string userId);

        [OperationContract]
        List<String> GetOrdersPoList(string userId);

        [OperationContract]
        Quote Get(long quoteId);

        [OperationContract]
        Quote Update(string userId, Quote quote);

        [OperationContract]
        void ConvertToOrder(string userId, long quoteId, string orderNumber);

        [OperationContract]
        long Copy(string creatorId, string newOwnerId, long fromQuoteId, string newName, string note, bool makeTemplate);

        [OperationContract]
        long CopyQuickQuote(string userId, long fromQuoteId, string workGroupId);

        [OperationContract]
        void AddDoorToQuote(long quoteId, long designId, string userId);

        [OperationContract]
        QuickQuote CreateQuoteFromQuickQuote(long designId, string userId, long detailId);

        [OperationContract]
        bool Delete(long id, string userId);

        [OperationContract]
        int CreateUploadAttachment(string conversationID, Int64 sourceDocId ,string messageId, string filePath, string fileType, string fileName);


    }

    [DataContract]
    public class Quote
    {
        public Quote()
        {
            Owner = new User();
            CreatedBy = new User();
            ModifiedBy = new User();
        }

        [DataMember]
        public User Owner { get; set; }

        [DataMember]
        public long QuoteId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public short BuildNumber { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string StatusDescription { get; set; }

        [DataMember]
        public string CatalogPrice { get; set; }

        [DataMember]
        public double SalePrice { get; set; }

        [DataMember]
        public string DealerPrice { get; set; }

        [DataMember]
        public long ProjectId { get; set; }

        [DataMember]
        public User CreatedBy { get; set; }

        [DataMember]
        public User ModifiedBy { get; set; }

        [DataMember]
        public string OriginType { get; set; }

        [DataMember]
        public string QuoteType { get; set; }

        [DataMember]
        public int TemplateId { get; set; }

        [DataMember]
        public int PriceBookId { get; set; }

        [DataMember]
        public long ChildId { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime ModifyDate { get; set; }

        [DataMember]
        public DateTime StatusDate { get; set; }

        [DataMember]
        public DateTime ExpireDate { get; set; }

        [DataMember]
        public DateTime ViewDate { get; set; }

        [DataMember]
        public DateTime BuildDate { get; set; }

        [DataMember]
        public DateTime ForecastDate { get; set; }

        [DataMember]
        public string Probability { get; set; }

        [DataMember]
        public string OpportunityLost { get; set; }

        [DataMember]
        public string ForecastOver1 { get; set; }

        [DataMember]
        public string ForecastOver2 { get; set; }

        [DataMember]
        public string ForecastOver3 { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string CurrencyFactor { get; set; }

        [DataMember]
        public bool QuoteValid { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public string OrderRefNumber { get; set; }

        [DataMember]
        public string Locale { get; set; }

        [DataMember]
        public long UserCustomerId { get; set; }

        [DataMember]
        public string UserCustomerName { get; set; }

        [DataMember]
        public string UserCustomerAccountNumber { get; set; }

        [DataMember]
        public string CustomerEmailAddress { get; set; }

        [DataMember]
        public long UserCustomerBillToAddressId { get; set; }

        [DataMember]
        public long UserCustomerShipToAddressId { get; set; }

        [DataMember]
        public string ShipToAddressLine1 { get; set; }

        [DataMember]
        public bool Archived { get; set; }

        [DataMember]
        public DateTime ArchivedDate { get; set; }

        [DataMember]
        public string ArchivedBy { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public double ShippingAmt { get; set; }

        [DataMember]
        public string ShippingType { get; set; }

        [DataMember]
        public double TaxAmt { get; set; }

        [DataMember]
        public string TaxType { get; set; }

        [DataMember]
        public double DepositPercentage { get; set; }

        [DataMember]
        public string PONumber { get; set; }

        [DataMember]
        public string PricingMethod { get; set; }

        [DataMember]
        public string PricingDefault { get; set; }

        [DataMember]
        public string PricingType { get; set; }

        [DataMember]
        public double TaxAmount { get; set; }

        [DataMember]
        public double ShippingAmount { get; set; }

        [DataMember]
        public int BlueprintId { get; set; }

        [DataMember]
        public string DealerPricingMethod { get; set; }

        [DataMember]
        public string DealerPricingDefault { get; set; }

        [DataMember]
        public string DealerPricingType { get; set; }
		[DataMember]
		public string DistributorName { get; set; }
    }

    [DataContract]
    public class QuickQuote
    {
        // This is how Max 2.0 find the product categroy
        private const int EntryId = 146817281; // Entry Doors - Global Door Options
        private const int InteriroId = 112515511; //Interior Doors - Global Door Options
        private const int PatioId = 168740121; // Patio Doors - Global Door Options

        private string getDoorExtension()
        {
            String extension = "";
            if (ProductId == InteriroId)
            {
                extension = "_INT";
            }
            else if (ProductId == PatioId)
            {
                extension = "_PATIO";
            }

            return extension;
        }
        //

        private string BuildImageUrl()
        {
            return "/HTML/products/" + ProductId.ToString() + 
            "/" + DesignUserId + 
            "/" + DesignId + 
            "/DRAWINGS/" + ReferenceNumber + "_REALTIME2D_Global_door_Image" + getDoorExtension() + ".png";
        }

        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public int DetailId { get; set; }

        [DataMember]
        public string DesignId { get; set; }

        [DataMember]
        public string ReferenceNumber { get; set; }

        [DataMember]
        public long DocumentId { get; set; }

        [DataMember]
        public string UnitPrice { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string DesignUserId { get; set; }

        [DataMember]
        public int ProductId { get; set; }

        [DataMember]
        public string DesignInputName { get; set; }

        [DataMember]
        public string DesignInputValue { get; set; }

        [DataMember]
        public string DesignInputNameCatalog { get; set; }

        [DataMember]
        public string DesignInputValueCatalog { get; set; }

        [DataMember]
        public string ImageUrl 
        {
            get { return BuildImageUrl(); }
            set { ImageUrl = value; }
        }
    }

}
