﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using Masonite.MTier.Utility;
using Masonite.MTier.Utility.SqlClient;

namespace MaxWCF
{
    public class CustomerEmailService : ICustomerEmailService
    {
        public CustomerEmail Get(string messageId)
        {
            CustomerEmail item = new CustomerEmail();
            if (!string.IsNullOrEmpty(messageId))
            {
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_Message"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", messageId.Length, messageId));

                    // Execute
                    using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        if ((reader != null) && reader.Read())
                        {
                            item.MessageId = reader.GetValueAsString("MESSAGE_ID");
                            item.ConversationId = reader.GetValueAsString("CONVERSATION_ID");
                            item.Body = reader.GetValueAsString("BODY");
                            item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                            item.Subject = reader.GetValueAsString("SUBJECT");
                            item.MessageType = reader.GetValueAsString("TYPE");
                        }
                    }
                }
            }
            return item;
        }

        public List<CustomerEmail> GetCustomerEmails(string userId)
        {
            List<CustomerEmail> items = new List<CustomerEmail>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_CustomerEmails"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            CustomerEmail item = new CustomerEmail();

                            item.ConversationId = reader.GetValueAsString("CONVERSATION_ID");
                            item.MessageId = reader.GetValueAsString("MESSAGE_ID");
                            item.Body = reader.GetValueAsString("BODY");
                            item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                            item.MessageCustomerId = reader.GetValueAsInt64("MessageCustomerId");
                            item.MessageOwner = reader.GetValueAsString("MessageOwner");
                            item.MessageStatus = reader.GetValueAsString("MessageStatus");
                            item.Subject = reader.GetValueAsString("SUBJECT");
                            item.MessageType = reader.GetValueAsString("TYPE");
                            item.SourceDocumentId = reader.GetValueAsInt64("SOURCE_DOC_ID");
                            item.SourceTypeCode = reader.GetValueAsString("SOURCE_TYPE_CD");
                            item.RecipientName = reader.GetValueAsString("RecipientName");
                            item.RecipientId = reader.GetValueAsString("RECIPIENT_ID");
                            item.RecipientEmailAddress = reader.GetValueAsString("RecipientEmail");
                            item.RecipientStatus = reader.GetValueAsString("RecipientStatus");
                            item.RecipientUserCustomerId = reader.GetValueAsInt64("RecipientUserCustomerId");
                            item.RecipientUserId = reader.GetValueAsString("RecipientUserId");
                            item.QuoteName = reader.GetValueAsString("QuoteName");

                            // Add to collection
                            items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public List<CustomerEmail> GetConversationMessages(string conversationId)
        {
            List<CustomerEmail> items = new List<CustomerEmail>();

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_ConversationMessages"))
            {
                command.CommandType = System.Data.CommandType.StoredProcedure;

                // Params
                command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationId));

                // Execute
                using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                {
                    // Populate local object
                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            CustomerEmail item = new CustomerEmail();

                            item.ConversationId = reader.GetValueAsString("CONVERSATION_ID");
                            item.MessageId = reader.GetValueAsString("MESSAGE_ID");
                            item.Body = reader.GetValueAsString("BODY");
                            item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                            item.MessageCustomerId = reader.GetValueAsInt64("MessageCustomerId");
                            item.MessageOwner = reader.GetValueAsString("MessageOwner");
                            item.MessageStatus = reader.GetValueAsString("MessageStatus");
                            item.Subject = reader.GetValueAsString("SUBJECT");
                            item.MessageType = reader.GetValueAsString("TYPE");
                            //item.CreateDate = reader.GetValueAsDate("ConversationCreateDate");
                            item.SourceDocumentId = reader.GetValueAsInt64("SOURCE_DOC_ID");
                            item.SourceTypeCode = reader.GetValueAsString("SOURCE_TYPE_CD");
                            item.RecipientName = reader.GetValueAsString("RecipientName");
                            item.OwnerFirstName = reader.GetValueAsString("OwnerFirstName");
                            item.OwnerMiddleInitial = reader.GetValueAsString("OwnerMiddleInitial");
                            item.OwnerLastName = reader.GetValueAsString("OwnerLastName");
                            item.OwnerEmail = reader.GetValueAsString("OwnerEmail");
                            item.RecipientId = reader.GetValueAsString("RECIPIENT_ID");
                            item.RecipientEmailAddress = reader.GetValueAsString("RecipientEmail");
                            item.RecipientStatus = reader.GetValueAsString("RecipientStatus");
                            item.RecipientUserCustomerId = reader.GetValueAsInt64("RecipientUserCustomerId");
                            item.RecipientUserId = reader.GetValueAsString("RecipientUserId");
                            item.QuoteName = reader.GetValueAsString("QuoteName");
							item.QuoteSerialNumber = reader.GetValueAsString("SerialNumber");

							// Add to collection
							items.Add(item);
                        }
                    }
                }
            }
            return items;
        }

        public List<MessageAttachment> GetQuoteAttachments(long quoteId)
        {
            List<MessageAttachment> items = new List<MessageAttachment>();
            try
            {
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_QuoteMessageAttachments"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@QuoteId", quoteId));

                    // Execute
                    using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Populate local object
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                MessageAttachment item = new MessageAttachment();

                                item.ConversationId = reader.GetValueAsString("CONVERSATION_ID");
                                item.MessageId = reader.GetValueAsString("MESSAGE_ID");
                                item.FilePath = reader.GetValueAsString("FILE_PATH");
                                item.FileType = reader.GetValueAsString("FILE_TYPE");
                                item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                                item.FileName = reader.GetValueAsString("FILE_NAME");

                                // Add to collection
                                items.Add(item);
                            }
                        }
                    }
                }
                return items;
            }
            catch (Exception ex)
            {
                string errorForTesting = ex.Message;
                throw;
            }
        }
        
        public List<MessageAttachment> GetConversationMessageAttachments(string conversationId, string messageId)
        {
            List<MessageAttachment> items = new List<MessageAttachment>();
            try
            {
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_get_ConversationMessageAttachments"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, messageId));

                    // Execute
                    using (SqlDataReader reader = SqlFactory.GetReader(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString))
                    {
                        // Populate local object
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                MessageAttachment item = new MessageAttachment();

                                item.ConversationId = reader.GetValueAsString("CONVERSATION_ID");
                                item.MessageId = reader.GetValueAsString("MESSAGE_ID");
                                item.FilePath = reader.GetValueAsString("FILE_PATH");
                                item.FileType = reader.GetValueAsString("FILE_TYPE");
                                item.CreateDate = reader.GetValueAsDate("CREATE_DATE");
                                item.FileName = reader.GetValueAsString("FILE_NAME");

                                // Add to collection
                                items.Add(item);
                            }
                        }
                    }
                }
                return items;
            }
            catch (Exception ex)
            {
                string errorForTesting = ex.Message;
                throw;
            }
        }

        public bool InsertConversationMessage(CustomerEmail conversationMessage, string userId)
        {
            //if (string.IsNullOrEmpty(savedBy)) { ExceptionHelper.ThrowNewArgumentNullException(ValidationMessage.SavedByIsRequired, "savedBy", System.Globalization.CultureInfo.CurrentCulture); }

            //Validate();

            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_ConversationMessage"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // CustomerEmail Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@UserId", 12, userId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, conversationMessage.MessageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationMessage.ConversationId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Subject", 100, conversationMessage.Subject));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Body", 2000, conversationMessage.Body));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Owner", 12, conversationMessage.MessageOwner));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Type", 10, conversationMessage.MessageType));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@MessageStatus", 1, conversationMessage.MessageStatus));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@CustomerId", conversationMessage.MessageCustomerId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@RecipientId", 40, conversationMessage.RecipientId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Email", 50, conversationMessage.RecipientEmailAddress));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@UserCustomerId", conversationMessage.RecipientUserCustomerId, false));
                    command.Parameters.Add(SqlFactory.BuildParameterChar("@RecipientStatus", 1, conversationMessage.RecipientStatus));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@RecipientUserId", 12, conversationMessage.RecipientUserId));
                    command.Parameters.Add(SqlFactory.BuildParameterBigInt("@SourceDocId", conversationMessage.SourceDocumentId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@SourceTypeCode", 10, conversationMessage.SourceTypeCode));


                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            catch (Exception ex)
            {
                string errorForTesting = ex.Message;
                throw;
            }
            finally
            {

            }
        }

        public bool InsertConversationMessageAttachment(string conversationId, string messageId, string filePath, string fileName, string fileType = "")
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_insert_ConversationMessageAttachment"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // CustomerEmail Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, messageId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@ConversationId", 40, conversationId));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filepath", 255, filePath));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@Filetype", 40, fileType));
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@FileName", 40, fileName));


                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            catch (Exception ex)
            {
                string errorForTesting = ex.Message;
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteCustomerEmail(string messageId)
        {
            try
            {
                // Command
                using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("usp_delete_CustomerEmail"))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    // Params
                    command.Parameters.Add(SqlFactory.BuildParameterVarChar("@MessageId", 40, messageId));

                    // Execute
                    return SqlFactory.GetNonQuery(command, System.Configuration.ConfigurationManager.ConnectionStrings["ConfiguratorConnectionString"].ConnectionString);
                }
            }
            //catch (Exception ex)
            //{
            //    Logging.LoggingHelper.Log(ex.ToString());
            //    throw;
            //}
            finally
            {

            }
        }
    }
}
