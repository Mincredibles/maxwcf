﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MaxWCF
{
    [ServiceContract]
    public interface ISalesLeadService
    {
        [OperationContract]
        List<SalesLead> GetSalesLeads(string workGroupId);

        [OperationContract]
        SalesLeadSummary GetSalesLeadSummary(string workGroupId);

        [OperationContract]
        SalesLead Get(long designId);

        [OperationContract]
        long CreateQuoteFromSalesLead(long designId, string userId);
    }

    [DataContract]
    public class SalesLead
    {
        public SalesLead()
        {
            SalesLeadCustomer = new Customer();
        }

        [DataMember]
        public string WorkGroupId { get; set; }

        [DataMember]
        public string DesignId { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime ConfiguredDate { get; set; }

        [DataMember]
        public double SalesLeadValue { get; set; }

        [DataMember]
        public Customer SalesLeadCustomer { get; set; }
    }

    [DataContract]
    public class SalesLeadSummary
    {
        [DataMember]
        public int SalesLeadCount { get; set; }

        [DataMember]
        public double SalesLeadValue { get; set; }

        [DataMember]
        public string SummaryDisplay { get; set; }

    }
}
