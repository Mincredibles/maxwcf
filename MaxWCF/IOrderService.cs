﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IOrderService
    {
        [OperationContract]
        List<Order> GetOrders(string userId);

        [OperationContract]
        Order Get(long orderId);

        [OperationContract]
        bool Delete(long id, string userId, string description);
    }


    [DataContract]
    public class Order
    {
        public Order()
        {
            Owner = new User();
        }

        [DataMember]
        public User Owner { get; set; }

        [DataMember]
        public string OwnerFirstName { get; set; }

        [DataMember]
        public string OwnerLastName { get; set; }

        [DataMember]
        public long OrderId { get; set; }

        [DataMember]
        public string CreatedByUserId { get; set; }

        [DataMember]
        public string OriginType { get; set; }

        [DataMember]
        public string OrderType { get; set; }

        [DataMember]
        public string CatalogPrice { get; set; }

        [DataMember]
        public string SalePrice { get; set; }

        [DataMember]
        public string POPrice { get; set; }

        [DataMember]
        public long CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public long CustomerBillToAddressId { get; set; }

        [DataMember]
        public long CustomerShipToAddressId { get; set; }

        [DataMember]
        public int PriceBookId { get; set; }

        [DataMember]
        public int ChildId { get; set; }

        [DataMember]
        public string Currency { get; set; }

        [DataMember]
        public string CurrencyFactor { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public string OrderRefNumber { get; set; }

        [DataMember]
        public int TemplateId { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string StatusDescription { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime ModifyDate { get; set; }

        [DataMember]
        public DateTime StatusDate { get; set; }

        [DataMember]
        public DateTime ProcessDate { get; set; }

        [DataMember]
        public string Process { get; set; }

        [DataMember]
        public string ModifiedByUserId { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public int ProjectId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Locale { get; set; }

        [DataMember]
        public string PONumber { get; set; }

        [DataMember]
        public string TaxAmount { get; set; }

        [DataMember]
        public string ShippingAmount { get; set; }
    }
}
