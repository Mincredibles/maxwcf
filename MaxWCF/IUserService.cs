﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Masonite.MTier.Utility;

namespace MaxWCF
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        List<User> GetList(string userId);

        [OperationContract]
        User Get(string userId);
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string MiddleInitial { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string QuoteDuration { get; set; }

        [DataMember]
        public string PricingMethod { get; set; }

        [DataMember]
        public string PricingDefault { get; set; }

        [DataMember]
        public string PricingType { get; set; }

        [DataMember]
        public string WorgroupDescription { get; set; }

        [DataMember]
        public string DealerSupportEmail { get; set; }

        [DataMember]
        public string DealerPOEmail { get; set; }

        [DataMember]
        public string ParentWorgroupDescription { get; set; }

        [DataMember]
        public string PriceBookName { get; set; }

        [DataMember]
        public string ParentPriceBookName { get; set; }

        [DataMember]
        public long QuickquoteTemplate { get; set; }
    }
}
